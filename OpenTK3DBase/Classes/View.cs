﻿using OpenTK;
using OpenTK.Graphics.OpenGL;
using OpenTK.Input;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenTK3DBase
{
    public class View
    {
        private Vector3 position, target, upVector;
        private float fieldOfView;
        private float nearClip, farClip;
        private bool orthogonal;
        public float orthogonalZoom;

        public Vector3 Position
        {
            get { return position; }
            set { this.position = value; }
        }
        public Vector3 Target
        {
            get { return target; }
            set { this.target = value; }
        }
        public Vector3 ForwardVector
        {
            get
            {
                return Vector3.Normalize(target - position);
            }
        }
        public Vector3 UpVector
        {
            get { return upVector; }
            set { this.upVector = value; }
        }
        public float FieldOfView
        {
            get { return fieldOfView; }
            set { this.fieldOfView = value; }
        }
        public float NearClip
        {
            get { return nearClip; }
            set { this.nearClip = value; }
        }
        public float FarClip
        {
            get { return farClip; }
            set { this.farClip = value; }
        }
        public bool Orthogonal
        {
            get { return orthogonal; }
            set { orthogonal = value; }
        }
        /// <summary>
        /// This matrix handles the viewport frustrum scaling
        /// </summary>
        public Matrix4 ProjMatrix
        {
            get
            {
                if (orthogonal)
                {
                    Frustum frustum = this.Frustum;
                    return Matrix4.CreateOrthographic(OrthoViewWidth, OrthoViewHeight, nearClip, farClip);
                }
                else
                {
                    return Matrix4.CreatePerspectiveFieldOfView(fieldOfView, (float)My.window.ClientSize.Width / My.window.ClientSize.Height, nearClip, farClip);
                }
            }
        }
        /// <summary>
        /// This matrix places the view at position looking at target with upVector
        /// </summary>
        public Matrix4 ViewMatrix
        {
            get
            {
                return Matrix4.LookAt(position, target, upVector);
            }
        }
        /// <summary>
        /// Uses My.window to determine AspectRatio
        /// </summary>
        public float AspectRatio
        {
            get
            {
                return (float)My.window.Bounds.Width / My.window.Bounds.Height;
            }
        }
        public Frustum Frustum
        {
            get
            {
                return new Frustum(position, target, upVector, fieldOfView, nearClip, farClip, AspectRatio);
            }
        }
        public float OrthoViewWidth
        {
            get
            {
                return orthogonalZoom * My.window.ClientSize.Width;
            }
        }
        public float OrthoViewHeight
        {
            get
            {
                return orthogonalZoom * My.window.ClientSize.Height;
            }
        }

        public View(Vector3 position, Vector3 target, Vector3 upVector,
            float feildOfView = MathHelper.PiOver2, float nearClip = 0.1f, float farClip = 100)
        {
            this.position = position;
            this.target = target;
            this.upVector = upVector;
            this.fieldOfView = feildOfView;
            this.nearClip = nearClip;
            this.farClip = farClip;
            this.orthogonal = false;
        }

        public void Update()
        {

        }

        public bool firstMovementCall = true;
        /// <summary>
        /// Only works if the upVector is (0, 1, 0)
        /// </summary>
        /// <param name="moveSpeed">the speed at which the camera moves through the world</param>
        /// <param name="movement">Whether to use WASD to allow movement of the camera</param>
        /// <param name="looking">Whether to use the mouse to allow rotation of the view</param>
        /// <param name="lookSpeed">1f is default 500 pixels per 180 degree turn</param>
        public void BasicMovement(float moveSpeed, bool movement = true, bool looking = true, float lookSpeed = 1f)
        {
            float sphereRadius = (target - position).Length;
            float targetHeight = Vector3.Dot(target - position, upVector);
            Vector3 rotationalPoint = target - upVector * targetHeight;
            float circleRadius = (rotationalPoint - position).Length;

            float vertAngle = (float)Math.Asin(targetHeight / sphereRadius);
            float horiAngle = (float)Math.Atan2(rotationalPoint.Z - position.Z, rotationalPoint.X - position.X);

            Vector3 forwardVector = Vector3.Normalize(rotationalPoint - position);
            Vector3 rightVector = Vector3.Cross(forwardVector, upVector);

            if (movement)
            {
                if (My.KeyDown(Key.ShiftLeft))
                    moveSpeed *= 2f;
                if (My.KeyDown(Key.ControlLeft))
                    moveSpeed *= 0.5f;
                if (My.KeyDown(OpenTK.Input.Key.W))
                {
                    position += forwardVector * moveSpeed;
                }
                if (My.KeyDown(OpenTK.Input.Key.S))
                {
                    position -= forwardVector * moveSpeed;
                }
                if (My.KeyDown(OpenTK.Input.Key.D))
                {
                    position += rightVector * moveSpeed;
                }
                if (My.KeyDown(OpenTK.Input.Key.A))
                {
                    position -= rightVector * moveSpeed;
                }
                if (My.KeyDown(OpenTK.Input.Key.E))
                {
                    position += upVector * moveSpeed;
                }
                if (My.KeyDown(OpenTK.Input.Key.Q))
                {
                    position -= upVector * moveSpeed;
                }
            }

            if (looking)
            {
                Vector2 offset = new Vector2(
                    My.window.Mouse.X - My.window.ClientSize.Width / 2,
                    -(My.window.Mouse.Y - My.window.ClientSize.Height / 2));//Y is negative because up is a positive angle movement
                if (firstMovementCall)//We'll just set the mouse position to the center of the screen when we first call BasicMovement
                {
                    firstMovementCall = false;
                    offset = Vector2.Zero;
                }

                Point center = new Point(My.window.ClientSize.Width / 2, My.window.ClientSize.Height / 2);
                Mouse.SetPosition(My.window.PointToScreen(center).X, My.window.PointToScreen(center).Y);

                horiAngle += (float)(offset.X / 500f * Math.PI * lookSpeed);
                vertAngle += (float)(offset.Y / 500f * Math.PI * lookSpeed);

                //Limit the vertical looking angle and normalize the horizontal
                float smallNum = 0.01f;//This is the closest we allow the player to look 90 degrees straight up or down
                if (vertAngle >= Math.PI / 2 - smallNum)
                    vertAngle = (float)Math.PI / 2 - smallNum;
                if (vertAngle <= -Math.PI / 2 + smallNum)
                    vertAngle = (float)-Math.PI / 2 + smallNum;
                while (horiAngle < 0)
                    horiAngle += (float)Math.PI * 2;
                while (horiAngle >= Math.PI * 2)
                    horiAngle -= (float)Math.PI * 2;

                //Find the new target from angles
                float newTargetY = sphereRadius * (float)Math.Sin(vertAngle);
                double newCircleRadius = sphereRadius * Math.Cos(vertAngle);
                float newTargetX = (float)newCircleRadius * (float)Math.Cos(horiAngle);
                float newTargetZ = (float)newCircleRadius * (float)Math.Sin(horiAngle);

                this.target = this.position + new Vector3(newTargetX, newTargetY, newTargetZ);
            }
        }

        /// <summary>
        /// Only works if the upVector is (0, 1, 0)
        /// </summary>
        /// <param name="moveSpeed">the speed at which the camera moves through the world</param>
        /// <param name="movement">Whether to use WASD to allow movement of the camera</param>
        /// <param name="looking">Whether to use the mouse to allow rotation of the view</param>
        /// <param name="lookSpeed">1f is default 500 pixels per 180 degree turn</param>
        public void RotateAround(Vector3 origin, bool movement = true, bool looking = true, float lookSpeed = 1f)
        {
            float sphereRadius = (target - position).Length;
            float targetHeight = Vector3.Dot(target - position, upVector);
            Vector3 rotationalPoint = target - upVector * targetHeight;
            float circleRadius = (rotationalPoint - position).Length;

            float vertAngle = (float)Math.Asin(targetHeight / sphereRadius);
            float horiAngle = (float)Math.Atan2(rotationalPoint.Z - position.Z, rotationalPoint.X - position.X);

            Vector3 forwardVector = Vector3.Normalize(rotationalPoint - position);
            Vector3 rightVector = Vector3.Cross(forwardVector, upVector);

            if (looking)
            {
                Vector2 offset = new Vector2(
                    My.window.Mouse.X - My.window.ClientSize.Width / 2,
                    -(My.window.Mouse.Y - My.window.ClientSize.Height / 2));//Y is negative because up is a positive angle movement
                if (firstMovementCall)//We'll just set the mouse position to the center of the screen when we first call BasicMovement
                {
                    firstMovementCall = false;
                    offset = Vector2.Zero;
                }

                Point center = new Point(My.window.ClientSize.Width / 2, My.window.ClientSize.Height / 2);
                Mouse.SetPosition(My.window.PointToScreen(center).X, My.window.PointToScreen(center).Y);

                horiAngle += (float)(offset.X / 500f * Math.PI * lookSpeed);
                vertAngle += (float)(offset.Y / 500f * Math.PI * lookSpeed);

                //Limit the vertical looking angle and normalize the horizontal
                float smallNum = 0.01f;//This is the closest we allow the player to look 90 degrees straight up or down
                if (vertAngle >= Math.PI / 2 - smallNum)
                    vertAngle = (float)Math.PI / 2 - smallNum;
                if (vertAngle <= -Math.PI / 2 + smallNum)
                    vertAngle = (float)-Math.PI / 2 + smallNum;
                while (horiAngle < 0)
                    horiAngle += (float)Math.PI * 2;
                while (horiAngle >= Math.PI * 2)
                    horiAngle -= (float)Math.PI * 2;

                //Find the new target from angles
                float newTargetY = sphereRadius * (float)Math.Sin(vertAngle);
                double newCircleRadius = sphereRadius * Math.Cos(vertAngle);
                float newTargetX = (float)newCircleRadius * (float)Math.Cos(horiAngle);
                float newTargetZ = (float)newCircleRadius * (float)Math.Sin(horiAngle);

                this.target = this.position + new Vector3(newTargetX, newTargetY, newTargetZ);
            }

            float distance = (this.position - origin).Length;
            this.position = origin - this.ForwardVector * distance;
        }

        public void ApplyTransforms()
        {
            Matrix4 projMatrix = ProjMatrix;
            GL.MatrixMode(MatrixMode.Projection);
            GL.LoadMatrix(ref projMatrix);

            Matrix4 viewMatrix = ViewMatrix;
            GL.MatrixMode(MatrixMode.Modelview);
            GL.LoadMatrix(ref viewMatrix);
        }

        public void EndTransforms()
        {

        }

        public Ray MouseRay
        {
            get
            {
                if (orthogonal)
                {
                    Vector2 mousePos = new Vector2(My.window.Mouse.X, My.window.Mouse.Y) - new Vector2(My.window.ClientRectangle.Width / 2f, My.window.ClientRectangle.Height / 2f);
                    mousePos.X = mousePos.X / (My.window.ClientRectangle.Width / 2f);
                    mousePos.Y = mousePos.Y / (My.window.ClientRectangle.Height / 2f);
                    Frustum frust = this.Frustum;
                    Vector3 start = this.position +
                        frust.TrueUpVector * -mousePos.Y * OrthoViewHeight / 2f +
                        frust.RightUnitVector * mousePos.X * OrthoViewWidth / 2f;
                    return new Ray(start, start + frust.TargetUnitVector);

                }
                else
                {
                    Vector2 mousePos = new Vector2(My.window.Mouse.X, My.window.Mouse.Y) - new Vector2(My.window.ClientRectangle.Width / 2f, My.window.ClientRectangle.Height / 2f);
                    mousePos.X = mousePos.X / (My.window.ClientRectangle.Width / 2f);
                    mousePos.Y = mousePos.Y / (My.window.ClientRectangle.Height / 2f);
                    Vector3 nearClipCenter = this.position + this.ForwardVector * this.nearClip;
                    Vector3 farClipCenter = this.position + this.ForwardVector * this.farClip;
                    Frustum frust = this.Frustum;
                    Vector3 rayTarget = farClipCenter +
                        mousePos.X * frust.RightUnitVector * frust.FarPlaneWidth / 2f +
                        -mousePos.Y * frust.TrueUpVector * frust.FarPlaneHeight / 2f;

                    return new Ray(this.position, rayTarget);
                }
            }
        }
    }
}
