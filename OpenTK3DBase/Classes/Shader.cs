﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK.Graphics.OpenGL;
using OpenTK;

namespace OpenTK3DBase
{
    /// <summary>
    /// This is my own interface for Shader's and defines the requirements
    /// a shader has to implement in order to be used in the Renderer class.
    /// </summary>
    public class Shader
    {
        /// <summary>
        /// This is the actual program that has been compiled
        /// </summary>
        protected ShaderProgram program;

        /// <summary>
        /// A local bitwise combined flag group that defines which buffers the
        /// shader expects.
        /// </summary>
        protected int bufferTypeFlags;


        /// <summary>
        /// This is the actual program that has been compiled
        /// </summary>
        public ShaderProgram Program { get { return program; } }


        /// <summary>
        /// A bitwise combined flag group that defines which buffers the
        /// shader expects.
        /// </summary>
        public int BufferTypeFlags { get { return bufferTypeFlags; } }
        /// <summary>
        /// This is an accessor for a single bit in the bufferTypeFlags variable
        /// </summary>
        public bool UsesIndexBuffer
        {
            get
            {
                return (bufferTypeFlags & (int)BufferType.Index) > 0;
            }
            set
            {
                if (value)
                    bufferTypeFlags = (bufferTypeFlags | (int)BufferType.Index);
                else
                    bufferTypeFlags = (bufferTypeFlags & ~(int)BufferType.Index);
            }
        }
        /// <summary>
        /// This is an accessor for a single bit in the bufferTypeFlags variable
        /// </summary>
        public bool UsesNormalBuffer
        {
            get
            {
                return (bufferTypeFlags & (int)BufferType.Normal) > 0;
            }
            set
            {
                if (value)
                    bufferTypeFlags = (bufferTypeFlags | (int)BufferType.Normal);
                else
                    bufferTypeFlags = (bufferTypeFlags & ~(int)BufferType.Normal);
            }
        }
        /// <summary>
        /// This is an accessor for a single bit in the bufferTypeFlags variable
        /// </summary>
        public bool UsesTexCoordBuffer
        {
            get
            {
                return (bufferTypeFlags & (int)BufferType.TexCoord) > 0;
            }
            set
            {
                if (value)
                    bufferTypeFlags = (bufferTypeFlags | (int)BufferType.TexCoord);
                else
                    bufferTypeFlags = (bufferTypeFlags & ~(int)BufferType.TexCoord);
            }
        }
        /// <summary>
        /// This is an accessor for a single bit in the bufferTypeFlags variable
        /// </summary>
        public bool UsesColorBuffer
        {
            get
            {
                return (bufferTypeFlags & (int)BufferType.Color) > 0;
            }
            set
            {
                if (value)
                    bufferTypeFlags = (bufferTypeFlags | (int)BufferType.Color);
                else
                    bufferTypeFlags = (bufferTypeFlags & ~(int)BufferType.Color);
            }
        }
        /// <summary>
        /// This is an accessor for a single bit in the bufferTypeFlags variable
        /// </summary>
        public bool UsesTangentBuffer
        {
            get
            {
                return (bufferTypeFlags & (int)BufferType.Tangent) > 0;
            }
            set
            {
                if (value)
                    bufferTypeFlags = (bufferTypeFlags | (int)BufferType.Tangent);
                else
                    bufferTypeFlags = (bufferTypeFlags & ~(int)BufferType.Tangent);
            }
        }
        /// <summary>
        /// Like Uses***Buffer accessors but general purpose.
        /// </summary>
        /// <param name="type">The type of buffer</param>
        /// <returns>Whether or not this Shader has been set to use that buffer</returns>
        public bool UsesBuffer(BufferType type) { return (bufferTypeFlags & (int)type) > 0; }


        /// <summary>
        /// Represents the attribute name in the shader that should
        /// be pointed at the position data in the buffer.
        /// Note: If not implemented by child class it will throw NotImplementedException when requested.
        /// This will be caught by the Renderer if considered to be a none vital input
        /// </summary>
        /// <returns>The name of an attribute to be set</returns>
        public virtual string GetPositionAttribName() { throw new NotImplementedException(); }
        /// <summary>
        /// Represents the attribute name in the shader that should
        /// be pointed at the normal data in the buffer.
        /// Note: If not implemented by child class it will throw NotImplementedException when requested.
        /// This will be caught by the Renderer if considered to be a none vital input
        /// </summary>
        /// <returns>The name of an attribute to be set</returns>
        public virtual string GetNormalAttribName() { throw new NotImplementedException(); }
        /// <summary>
        /// Represents the attribute name in the shader that should
        /// be pointed at the texCoord data in the buffer.
        /// Note: If not implemented by child class it will throw NotImplementedException when requested.
        /// This will be caught by the Renderer if considered to be a none vital input
        /// </summary>
        /// <returns>The name of an attribute to be set</returns>
        public virtual string GetTexCoordAttribName() { throw new NotImplementedException(); }
        /// <summary>
        /// Represents the attribute name in the shader that should
        /// be pointed at the color data in the buffer.
        /// Note: If not implemented by child class it will throw NotImplementedException when requested.
        /// This will be caught by the Renderer if considered to be a none vital input
        /// </summary>
        /// <returns>The name of an attribute to be set</returns>
        public virtual string GetColorAttribName() { throw new NotImplementedException(); }
        /// <summary>
        /// Represents the attribute name in the shader that should
        /// be pointed at the tangent data in the buffer.
        /// Note: If not implemented by child class it will throw NotImplementedException when requested.
        /// This will be caught by the Renderer if considered to be a none vital input
        /// </summary>
        /// <returns>The name of an attribute to be set</returns>
        public virtual string GetTangentAttribName() { throw new NotImplementedException(); }
        /// <summary>
        /// Represents the uniform name of the matrix that represents
        /// the World matrix.
        /// Note: If not implemented by child class it will throw NotImplementedException when requested.
        /// This will be caught by the Renderer if considered to be a none vital input
        /// </summary>
        /// <returns>The name of a uniform to be set</returns>
        public virtual string GetWorldMatrixName() { throw new NotImplementedException(); }
        /// <summary>
        /// Represents the uniform name of the matrix that represents
        /// the ModelView matrix.
        /// Note: If not implemented by child class it will throw NotImplementedException when requested.
        /// This will be caught by the Renderer if considered to be a none vital input
        /// </summary>
        /// <returns>The name of a uniform to be set</returns>
        public virtual string GetModelViewMatrixName() { throw new NotImplementedException(); }
        /// <summary>
        /// Represents the uniform name of the matrix that represents
        /// the Projection matrix.
        /// Note: If not implemented by child class it will throw NotImplementedException when requested.
        /// This will be caught by the Renderer if considered to be a none vital input
        /// </summary>
        /// <returns>The name of a uniform to be set</returns>
        public virtual string GetProjectionMatrixName() { throw new NotImplementedException(); }
        /// <summary>
        /// Represents the uniform name of the matrix that represents
        /// the combination of World, ModelView, and Projection matrices.
        /// Note: If not implemented by child class it will throw NotImplementedException when requested.
        /// This will be caught by the Renderer if considered to be a none vital input
        /// </summary>
        /// <returns>The name of a uniform to be set</returns>
        public virtual string GetCombinedMatrixName() { throw new NotImplementedException(); }
        /// <summary>
        /// Represents the uniform name of the texture sampler that represents
        /// the diffuse texture.
        /// Note: If not implemented by child class it will throw NotImplementedException when requested.
        /// This will be caught by the Renderer if considered to be a none vital input
        /// </summary>
        /// <returns>The name of a uniform to be set</returns>
        public virtual string GetTextureName() { throw new NotImplementedException(); }
        /// <summary>
        /// Represents the uniform name of the Vector3 that represents
        /// the position of the view.
        /// Note: If not implemented by child class it will throw NotImplementedException when requested.
        /// This will be caught by the Renderer if considered to be a none vital input
        /// </summary>
        /// <returns>The name of a uniform to be set</returns>
        public virtual string GetViewPosName() { throw new NotImplementedException(); }
        /// <summary>
        /// Represents the uniform name of first light in the shader code.
        /// Note: If not implemented by child class it will throw NotImplementedException when requested.
        /// This will be caught by the Renderer if considered to be a none vital input.
        /// 
        /// A light, as defined by Light.cs has these properties. (these are literally strings we will look for in SetLight0())
        /// isOn,
        /// isInfinite,
        /// position,
        /// diffuseColor,
        /// specularColor,
        /// constantAttenuation,
        /// linearAttenuation,
        /// quadraticAttenuation,
        /// spotCutoff,
        /// spotExponent,
        /// spotDirection,
        /// </summary>
        /// <returns>The name of a uniform to be set</returns>
        public virtual string GetLight0Name() { throw new NotImplementedException(); }


        /// <summary>
        /// Calls Gl.VertexAttribPointer with the correct name returned by the virtual function
        /// </summary>
        public void SetPositionPointer(int byteOffset, int byteStride)
        {
            GL.VertexAttribPointer(
                program.GetAttribute(GetPositionAttribName()), 
                3, VertexAttribPointerType.Float, false,
                byteStride,  byteOffset);
        }
        /// <summary>
        /// Calls Gl.VertexAttribPointer with the correct name returned by the virtual function
        /// </summary>
        public void SetNormalPointer(int byteOffset, int byteStride)
        {
            GL.VertexAttribPointer(
                program.GetAttribute(GetNormalAttribName()),
                3, VertexAttribPointerType.Float, false,
                byteStride, byteOffset);
        }
        /// <summary>
        /// Calls Gl.VertexAttribPointer with the correct name returned by the virtual function
        /// </summary>
        public void SetTexCoordPointer(int byteOffset, int byteStride)
        {
            GL.VertexAttribPointer(
                program.GetAttribute(GetTexCoordAttribName()),
                2, VertexAttribPointerType.Float, false,
                byteStride, byteOffset);
        }
        /// <summary>
        /// Calls Gl.VertexAttribPointer with the correct name returned by the virtual function
        /// </summary>
        public void SetColorPointer(int byteOffset, int byteStride)
        {
            GL.VertexAttribPointer(
                program.GetAttribute(GetColorAttribName()),
                4, VertexAttribPointerType.Float, false,
                byteStride, byteOffset);
        }
        /// <summary>
        /// Calls Gl.VertexAttribPointer with the correct name returned by the virtual function
        /// </summary>
        public void SetTangentPointer(int byteOffset, int byteStride)
        {
            GL.VertexAttribPointer(
                program.GetAttribute(GetTangentAttribName()),
                3, VertexAttribPointerType.Float, false,
                byteStride, byteOffset);
        }
        /// <summary>
        /// Sets the World matrix' value using UniformMatrix4 and this.GetWorldMatrixName.
        /// Note since this is a uniform it should be called after program.EnableVertexAttribArrays()
        /// </summary>
        /// <param name="world"></param>
        public void SetWorldMatrix(Matrix4 world)
        {
            GL.UniformMatrix4(program.GetUniform(this.GetWorldMatrixName()), false, ref world);
        }
        /// <summary>
        /// Sets the ModelView matrix' value using UniformMatrix4 and this.GetModelViewMatrixName.
        /// Note since this is a uniform it should be called after program.EnableVertexAttribArrays()
        /// </summary>
        /// <param name="modelview"></param>
        public void SetModelViewMatrix(Matrix4 modelview)
        {
            GL.UniformMatrix4(program.GetUniform(this.GetModelViewMatrixName()), false, ref modelview);
        }
        /// <summary>
        /// Sets the Projection matrix' value using UniformMatrix4 and this.GetProjectionMatrixName.
        /// Note since this is a uniform it should be called after program.EnableVertexAttribArrays()
        /// </summary>
        public void SetProjectionMatrix(Matrix4 projection)
        {
            GL.UniformMatrix4(program.GetUniform(this.GetProjectionMatrixName()), false, ref projection);
        }
        /// <summary>
        /// Sets the combined matrix' value using UniformMatrix4 and this.GetCombinedMatrixName.
        /// Note since this is a uniform it should be called after program.EnableVertexAttribArrays()
        /// </summary>
        /// <param name="combined"></param>
        public void SetCombinedMatrix(Matrix4 combined)
        {
            GL.UniformMatrix4(program.GetUniform(this.GetCombinedMatrixName()), false, ref combined);
        }
        /// <summary>
        /// Sets the texture's value using Uniform1 and this.GetTextureName.
        /// Note since this is a uniform it should be called after program.EnableVertexAttribArrays()
        /// </summary>
        public void SetTexture(Texture2D texture)
        {
            GL.ActiveTexture(TextureUnit.Texture0);
            GL.BindTexture(TextureTarget.Texture2D, texture.Id);
            GL.Uniform1(GL.GetUniformLocation(this.program.ProgramID, this.GetTextureName()), 0);
        }
        /// <summary>
        /// Sets the View Position value using Uniform3 and this.GetViewPosName.
        /// Note since this is a uniform it should be called after program.EnableVertexAttribArrays()
        /// </summary>
        public void SetViewPosition(Vector3 position)
        {
            GL.Uniform3(GL.GetUniformLocation(this.program.ProgramID, this.GetViewPosName()), position);
        }
        /// <summary>
        /// Sets the values tied to a light using GL.Uniform*() and this.GetLight0Name.
        /// 
        /// A light, as defined by Light.cs has these properties. (these are literally strings we will look for)
        /// isOn,
        /// isInfinite,
        /// position,
        /// diffuseColor,
        /// specularColor,
        /// constantAttenuation,
        /// linearAttenuation,
        /// quadraticAttenuation,
        /// spotCutoff,
        /// spotExponent,
        /// spotDirection,
        /// </summary>
        public void SetLight0(Light light0)
        {
            //TODO: Catch exceptions if a light doesn't implement all of these values
            GL.Uniform1(this.program.GetUniform(this.GetLight0Name() + ".isOn"), light0.isOn ? 1f : 0f);
            GL.Uniform1(this.program.GetUniform(this.GetLight0Name() + ".isInfinite"), light0.isInfinite ? 1f : 0f);
            GL.Uniform3(this.program.GetUniform(this.GetLight0Name() + ".position"), light0.position);
            GL.Uniform4(this.program.GetUniform(this.GetLight0Name() + ".diffuseColor"), light0.diffuseColor);
            GL.Uniform4(this.program.GetUniform(this.GetLight0Name() + ".specularColor"), light0.specularColor);
            GL.Uniform1(this.program.GetUniform(this.GetLight0Name() + ".constantAttenuation"), light0.conAttenuation);
            GL.Uniform1(this.program.GetUniform(this.GetLight0Name() + ".linearAttenuation"), light0.linAttenuation);
            GL.Uniform1(this.program.GetUniform(this.GetLight0Name() + ".quadraticAttenuation"), light0.quadAttenuation);
            GL.Uniform1(this.program.GetUniform(this.GetLight0Name() + ".spotCutoff"), light0.spotCutoff);
            GL.Uniform1(this.program.GetUniform(this.GetLight0Name() + ".spotExponent"), light0.spotExponent);
            GL.Uniform3(this.program.GetUniform(this.GetLight0Name() + ".spotDirection"), light0.spotDirection);
        }
    }
}
