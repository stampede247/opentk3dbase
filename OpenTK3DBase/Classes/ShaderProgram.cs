﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK.Graphics.OpenGL;
using System.IO;
using OpenTK;

namespace OpenTK3DBase
{
	public class ShaderProgram
	{
		public int ProgramID = -1,
			VShaderID = -1,
			FShaderID = -1,
			GShaderID = -1,
			AttributeCount = 0,
			UniformCount = 0;

		public Dictionary<string, AttributeInfo> Attributes;
		public Dictionary<string, UniformInfo> Uniforms;
		public Dictionary<string, uint> Buffers;

		public ShaderProgram()
		{
			this.ProgramID = GL.CreateProgram();
			Attributes = new Dictionary<string, AttributeInfo>();
			Uniforms = new Dictionary<string, UniformInfo>();
			Buffers = new Dictionary<string, uint>();
		}
		public ShaderProgram(string vShader, string fShader, bool fromFile = false)
		{
			ProgramID = GL.CreateProgram();

			Attributes = new Dictionary<string, AttributeInfo>();
			Uniforms = new Dictionary<string, UniformInfo>();
			Buffers = new Dictionary<string, uint>();

			if (fromFile)
			{
				LoadShaderFromFile(vShader, ShaderType.VertexShader);
				LoadShaderFromFile(fShader, ShaderType.FragmentShader);
			}
			else
			{
				LoadShaderFromString(vShader, ShaderType.VertexShader);
				LoadShaderFromString(fShader, ShaderType.FragmentShader);
			}
            
			Link();
			GenBuffers();
		}
		public ShaderProgram(string vShader, string gShader, string fShader, bool fromFile = false)
		{
			ProgramID = GL.CreateProgram();

			Attributes = new Dictionary<string, AttributeInfo>();
			Uniforms = new Dictionary<string, UniformInfo>();
			Buffers = new Dictionary<string, uint>();

			if (fromFile)
			{
				LoadShaderFromFile(vShader, ShaderType.VertexShader);
				LoadShaderFromFile(fShader, ShaderType.FragmentShader);
				LoadShaderFromFile(gShader, ShaderType.GeometryShader);
			}
			else
			{
				LoadShaderFromString(vShader, ShaderType.VertexShader);
				LoadShaderFromString(fShader, ShaderType.FragmentShader);
				LoadShaderFromString(gShader, ShaderType.GeometryShader);
			}

			Link();
			GenBuffers();
		}

		private void LoadShader(string code, ShaderType type, out int address)
		{
			address = GL.CreateShader(type);
			GL.ShaderSource(address, code);
			GL.CompileShader(address);
			GL.AttachShader(ProgramID, address);
			Console.WriteLine("Shader Loading Status: " + GL.GetShaderInfoLog(address));
		}
		public void LoadShaderFromString(string code, ShaderType type)
		{
			if (type == ShaderType.VertexShader)
			{
				LoadShader(code, type, out VShaderID);
			}
			else if (type == ShaderType.FragmentShader)
			{
				LoadShader(code, type, out FShaderID);
			}
			else if (type == ShaderType.GeometryShader)
			{
				LoadShader(code, type, out GShaderID);
			}
		}
		public void LoadShaderFromFile(string filename, ShaderType type)
		{
			using (StreamReader reader = new StreamReader(filename))
			{
				if (type == ShaderType.VertexShader)
				{
					LoadShader(reader.ReadToEnd(), type, out VShaderID);
				}
				else if (type == ShaderType.FragmentShader)
				{
					LoadShader(reader.ReadToEnd(), type, out FShaderID);
				}
				else if (type == ShaderType.GeometryShader)
				{
					LoadShader(reader.ReadToEnd(), type, out GShaderID);
				}
			}
		}

		public void Link()
		{
			GL.LinkProgram(ProgramID);

			Console.WriteLine("Shader Linking Status: " + GL.GetProgramInfoLog(ProgramID));

			GL.GetProgram(ProgramID, GetProgramParameterName.ActiveAttributes, out AttributeCount);
			GL.GetProgram(ProgramID, GetProgramParameterName.ActiveUniforms, out UniformCount);

            if (My.DebugEnabled) Console.WriteLine("{0} Attributes found.", AttributeCount);
            if (My.DebugEnabled) Console.WriteLine("{0} Uniforms found.", UniformCount);

            for (int i = 0; i < AttributeCount; i++)
			{
                AttributeInfo info = new AttributeInfo();
                int length = 0;

                StringBuilder name = new StringBuilder();
                GL.GetActiveAttrib(ProgramID, i, 256, out length, out info.size, out info.type, name);

                info.name = name.ToString();
                info.address = GL.GetAttribLocation(ProgramID, info.name);
                Attributes.Add(name.ToString(), info);
            }

            for (int i = 0; i < UniformCount; i++)
            {
                //UniformInfo info = new UniformInfo();
                //int length = 0;

                //StringBuilder name = new StringBuilder();

                //GL.GetActiveUniform(ProgramID, i, 512, out length, out info.size, out info.type, name);
                //Console.WriteLine("Uniform{0} - '{1}'", i, name);

                //if (length > 0)
                //{
                //    info.name = name.ToString();
                //    info.address = GL.GetUniformLocation(ProgramID, info.name);
                //    Uniforms.Add(name.ToString(), info);
                //    Console.WriteLine("Completed Uniform {0} - '{1}'", i, name);
                //}
                //else
                //{
                //    Console.WriteLine("Discarded Uniform{0}", i);
                //}
            }
		}

		public void GenBuffers()
		{
			for (int i = 0; i < Attributes.Count; i++)
			{
				uint buffer = 0;
				GL.GenBuffers(1, out buffer);

				Buffers.Add(Attributes.Values.ElementAt(i).name, buffer);
			}

			for (int i = 0; i < Uniforms.Count; i++)
			{
				uint buffer = 0;
				GL.GenBuffers(1, out buffer);

				Buffers.Add(Uniforms.Values.ElementAt(i).name, buffer);
			}
		}

		public void EnableVertexAttribArrays()
		{
			for (int i = 0; i < Attributes.Count; i++)
			{
				GL.EnableVertexAttribArray(Attributes.Values.ElementAt(i).address);
			}
		}
		public void DisableVertexAttribArrays()
		{
			for (int i = 0; i < Attributes.Count; i++)
			{
				GL.DisableVertexAttribArray(Attributes.Values.ElementAt(i).address);
			}
		}

		public int GetAttribute(string name)
		{
			if (Attributes.ContainsKey(name))
			{
				return Attributes[name].address;
			}
			else
			{
				return -1;
			}
		}
		public int GetUniform(string name)
		{
			if (Uniforms.ContainsKey(name))
			{
				return Uniforms[name].address;
			}
			else
			{
                try
                {
                    return GL.GetUniformLocation(ProgramID, name);
                }
                catch
                {
                    return -1;
                }
			}
		}
		public uint GetBuffer(string name)
		{
			if (Buffers.ContainsKey(name))
			{
				return Buffers[name];
			}
			else
			{
				return 0;
			}
		}
	}

    public class AttributeInfo
	{
		public string name = "";
		public int address = -1;
		public int size = 0;
		public ActiveAttribType type;
	}
    public class UniformInfo
	{
		public string name = "";
		public int address = -1;
		public int size = 0;
		public ActiveUniformType type;
	}
}
