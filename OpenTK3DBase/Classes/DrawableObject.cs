﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;
using System.Drawing;
using OpenTK.Graphics.OpenGL;

namespace OpenTK3DBase
{
    /// <summary>
    /// This is an enum used for the DrawableObject class to define
    /// which buffers you want the class to use. They are powers of
    /// two and can be bitwise added to use multiple.
    /// Also used for Shader class.
    /// </summary>
    public enum BufferType
    {
        None = 0,
        Index = 1,
        Normal = 2,
        TexCoord = 4,
        Color = 8,
        Tangent = 16,
        All = 0xffff,
    }

    /// <summary>
    /// Creates a vertex buffer (and index buffer if enabled) with different types of vertex data.
    /// The order of the attributes is Position, Normal, TexCoord, Color and Tangent.
    /// Note: In opengl there is only one "Buffer" for vertex data that contains all the different
    /// "Attributes". In this class we use the word "buffer" to refer to the different attribute arrays
    /// that will actually be combined into one buffer on OpenGL's end.
    /// Turns out OpenGL interprets an offset of 0 to mean "Tightly packed" rather than no offset. Can't
    /// find a way to change this so we're going to have to completely populate the buffer with all the
    /// possible data types for now
    /// </summary>
    public class DrawableObject
    {
        /// <summary>
        /// Defines the method with which this DrawableObject should be
        /// drawn in OpenGL. 
        /// </summary>
        public PrimitiveType primitiveType;

        /// <summary>
        /// The local copy of the vertices.
        /// This is not updated on OpenGL's side until CreateBuffers is called
        /// </summary>
        public Vector3[] vertices;
        /// <summary>
        /// The local copy of the indices.
        /// This is not updated on OpenGL's side until CreateBuffers is called
        /// </summary>
        public uint[] indices;
        /// <summary>
        /// The local copy of the normals.
        /// This is not updated on OpenGL's side until CreateBuffers is called
        /// </summary>
        public Vector3[] normals;
        /// <summary>
        /// The local copy of the texture coordinates.
        /// This is not updated on OpenGL's side until CreateBuffers is called
        /// </summary>
        public Vector2[] texCoords;
        /// <summary>
        /// The local copy of the colors.
        /// This is not updated on OpenGL's side until CreateBuffers is called
        /// </summary>
        public Vector4[] colors;
        /// <summary>
        /// The local copy of the tangents.
        /// This is not updated on OpenGL's side until CreateBuffers is called
        /// </summary>
        public Vector3[] tangents;

        /// <summary>
        /// These are inserted as the beginning of the vert buffer ONLY IF
        /// the flag for the corrisponding data type is off. This way if
        /// a drawing function needs a value that this drawableobject has
        /// no array for it can simply point to one value for all vertices
        /// </summary>
        public Vector3 defaultNormal;
        /// <summary>
        /// These are inserted as the beginning of the vert buffer ONLY IF
        /// the flag for the corrisponding data type is off. This way if
        /// a drawing function needs a value that this drawableobject has
        /// no array for it can simply point to one value for all vertices
        /// </summary>
        public Vector2 defaultTexCoord;
        /// <summary>
        /// These are inserted as the beginning of the vert buffer ONLY IF
        /// the flag for the corrisponding data type is off. This way if
        /// a drawing function needs a value that this drawableobject has
        /// no array for it can simply point to one value for all vertices
        /// </summary>
        public Vector4 defaultColor;
        /// <summary>
        /// These are inserted as the beginning of the vert buffer ONLY IF
        /// the flag for the corrisponding data type is off. This way if
        /// a drawing function needs a value that this drawableobject has
        /// no array for it can simply point to one value for all vertices
        /// </summary>
        public Vector3 defaultTangent;


        /// <summary>
        /// These are simply flags for the CreateBuffers function
        /// as well as outside classes who want to know what this
        /// DrawableObject contains.
        /// </summary>
        private int bufferTypeFlags;
        /// <summary>
        /// Tells us whether CreateBuffers has been called (at least once)
        /// </summary>
        private bool buffersCreated;

        /// <summary>
        /// This is the number of floats for each vertex in the vert buffer.
        /// Defaults to 0 until CreateBuffers is called
        /// </summary>
        private int vertexStride;
        /// <summary>
        /// This is the offset of the first vertex in the vert buffer.
        /// Defaults to 0 until CreateBuffers is called
        /// </summary>
        private int vertexOffset;
        /// <summary>
        /// This is the number of floats in the vertex buffer
        /// </summary>
        private int vertBufferSize;
        /// <summary>
        /// These are the OpenGL indexes for the vertex buffer and index buffer.
        /// (assuming they have been initialized with CreateBuffers())
        /// </summary>
        private int vertBufferID, indBufferID;

        /// <summary>
        /// Num floats
        /// </summary>
        private int attPositionOffset, attPositionStride,
            attNormalOffset, attNormalStride,
            attTexCoordOffset, attTexCoordStride,
            attColorOffset, attColorStride,
            attTangentOffset, attTangentStride;

        /// <summary>
        /// These are the OpenGL indexes for the vertex buffer and index buffer.
        /// (assuming they have been initialized with CreateBuffers())
        /// </summary>
        public int VertBufferID { get { return vertBufferID; } }
        /// <summary>
        /// These are the OpenGL indexes for the vertex buffer and index buffer.
        /// (assuming they have been initialized with CreateBuffers())
        /// </summary>
        public int IndBufferID { get { return indBufferID; } }

        /// <summary>
        /// A public accessor the the field that holds the flags of which
        /// buffers should be used
        /// </summary>
        public int BufferTypeFlags { get { return bufferTypeFlags; } }
        /// <summary>
        /// This is an accessor for a single bit in the bufferTypeFlags variable
        /// </summary>
        public bool HasIndexBuffer
        {
            get
            {
                return (bufferTypeFlags & (int)BufferType.Index) > 0;
            }
            set
            {
                if (value)
                    bufferTypeFlags = (bufferTypeFlags | (int)BufferType.Index);
                else
                    bufferTypeFlags = (bufferTypeFlags & ~(int)BufferType.Index);
            }
        }
        /// <summary>
        /// This is an accessor for a single bit in the bufferTypeFlags variable
        /// </summary>
        public bool HasNormalBuffer
        {
            get
            {
                return (bufferTypeFlags & (int)BufferType.Normal) > 0;
            }
            set
            {
                if (value)
                    bufferTypeFlags = (bufferTypeFlags | (int)BufferType.Normal);
                else
                    bufferTypeFlags = (bufferTypeFlags & ~(int)BufferType.Normal);
            }
        }
        /// <summary>
        /// This is an accessor for a single bit in the bufferTypeFlags variable
        /// </summary>
        public bool HasTexCoordBuffer
        {
            get
            {
                return (bufferTypeFlags & (int)BufferType.TexCoord) > 0;
            }
            set
            {
                if (value)
                    bufferTypeFlags = (bufferTypeFlags | (int)BufferType.TexCoord);
                else
                    bufferTypeFlags = (bufferTypeFlags & ~(int)BufferType.TexCoord);
            }
        }
        /// <summary>
        /// This is an accessor for a single bit in the bufferTypeFlags variable
        /// </summary>
        public bool HasColorBuffer
        {
            get
            {
                return (bufferTypeFlags & (int)BufferType.Color) > 0;
            }
            set
            {
                if (value)
                    bufferTypeFlags = (bufferTypeFlags | (int)BufferType.Color);
                else
                    bufferTypeFlags = (bufferTypeFlags & ~(int)BufferType.Color);
            }
        }
        /// <summary>
        /// This is an accessor for a single bit in the bufferTypeFlags variable
        /// </summary>
        public bool HasTangentBuffer
        {
            get
            {
                return (bufferTypeFlags & (int)BufferType.Tangent) > 0;
            }
            set
            {
                if (value)
                    bufferTypeFlags = (bufferTypeFlags | (int)BufferType.Tangent);
                else
                    bufferTypeFlags = (bufferTypeFlags & ~(int)BufferType.Tangent);
            }
        }
        /// <summary>
        /// Like Has***Buffer accessors but general purpose.
        /// </summary>
        /// <param name="type">The type of buffer</param>
        /// <returns>Whether or not this DrawableObject has been set to have that buffer</returns>
        public bool HasBuffer(BufferType type) { return (bufferTypeFlags & (int)type) > 0; }

        /// <summary>
        /// The number of vertices in our locally stored vertex array
        /// </summary>
        public int NumVertices { get { return vertices.Length; } }
        /// <summary>
        /// The number of indices in our locally stored index array
        /// </summary>
        public int NumIndices { get { return indices.Length; } }
        /// <summary>
        /// The number of normals in our locally stored normal array
        /// </summary>
        public int NumNormals { get { return normals.Length; } }
        /// <summary>
        /// The number of texture cordinates in our locally stored texture coordinate array
        /// </summary>
        public int NumTexCoords { get { return texCoords.Length; } }
        /// <summary>
        /// The number of colors in our locally stored color array
        /// </summary>
        public int NumColors { get { return colors.Length; } }
        /// <summary>
        /// The number of tangents in our locally stored tangent array
        /// </summary>
        public int NumTangents { get { return tangents.Length; } }

        /// <summary>
        /// This is the number of floats for each vertex in the vert buffer.
        /// Defaults to 0 until CreateBuffers is called
        /// </summary>
        public int VertexStride { get { return vertexStride; } }
        /// <summary>
        /// This is the offset of the first vertex in the vert buffer.
        /// Defaults to 0 until CreateBuffers is called
        /// </summary>
        public int VertexOffset { get { return vertexOffset; } }
        /// <summary>
        /// Tells us whether CreateBuffers function has been called (at least once)
        /// </summary>
        public bool BuffersCreated { get { return buffersCreated; } }


        /// <summary>
        /// Default constructor for DrawableObject. Simply sets the input
        /// parameters locally and intialized things that need to be
        /// initialized.
        /// </summary>
        /// <param name="primitiveType">The way this DrawableObject will be drawn through OpenGL</param>
        /// <param name="bufferTypes">A bitwise operator compatible flag set for which buffers are going to be used</param>
        public DrawableObject(PrimitiveType primitiveType, BufferType bufferTypes = BufferType.None)
        {
            this.primitiveType = primitiveType;
            this.bufferTypeFlags = (int)bufferTypes;
            this.vertexStride = 0;
            this.vertexOffset = 0;
            this.vertBufferID = -1;
            this.indBufferID = -1;
            this.vertBufferSize = 0;

            this.defaultNormal = new Vector3(0, 1, 0);
            this.defaultTexCoord = new Vector2(0, 0);
            this.defaultColor = new Vector4(1, 1, 1, 1);
            this.defaultTangent = new Vector3(1, 0, 0);
            this.buffersCreated = false;
        }
        /// <summary>
        /// Secondary constructor that just makes a bitwise combined flag to pass to other constructor.
        /// </summary>
        public DrawableObject(PrimitiveType primitiveType, bool hasIndexBuffer = false, 
            bool hasNormalBuffer = false, bool hasTexCoordBuffer = false, 
            bool hasColorBuffer = false, bool hasTangentBuffer = false)
            : this(primitiveType, 
                  (hasIndexBuffer ? BufferType.Index : BufferType.None) |
                  (hasNormalBuffer ? BufferType.Normal : BufferType.None) |
                  (hasTexCoordBuffer ? BufferType.TexCoord : BufferType.None) |
                  (hasColorBuffer ? BufferType.Color : BufferType.None) |
                  (hasTangentBuffer ? BufferType.Tangent : BufferType.None))
        {

        }


        /// <summary>
        /// Takes the current settings and passes all the information to a new (or previously created)
        /// buffer on OpenGL's side. The pointers to the buffers are stored in vertBufferID and indBufferID.
        /// </summary>
        /// <param name="usageHint">Passed to OpenGL when filling buffers</param>
        public void CreateBuffers(BufferUsageHint usageHint = BufferUsageHint.StaticDraw)
        {
            //Check to make sure all our attribute arrays (of all that are active) are the same size
            if ((HasNormalBuffer && NumNormals != NumVertices) ||
                (HasTexCoordBuffer && NumTexCoords != NumVertices) ||
                (HasColorBuffer && NumColors != NumVertices) ||
                (HasTangentBuffer && NumTangents != NumVertices))
                throw new Exception("Creating a buffer when the different arrays of data are not the same length.\n" +
                    "Vertices: " + NumVertices.ToString() + "\n" +
                    "Normals: " + NumNormals.ToString() + "\n" +
                    "TexCoords: " + NumTexCoords.ToString() + "\n" +
                    "Colors: " + NumColors.ToString() + "\n" +
                    "Tangents: " + NumTangents.ToString());

            if (vertBufferID == -1)//Generate the a space for the buffer if it hasn't been made yet
                vertBufferID = GL.GenBuffer();

            //vertexStride = 3 +//3 floats for position (vertex)
            //    (HasNormalBuffer ? 3 : 0) +
            //    (HasTexCoordBuffer ? 2 : 0) +
            //    (HasColorBuffer ? 4 : 0) +
            //    (HasTangentBuffer ? 3 : 0);

            vertexStride = 3 +//position
                3 +//normal
                2 +//texCoord
                4 +//color
                3;//tangent

            //first we have to figure out how many floats we need to reserve for default values so we can create the float array
            int numDefaultFloats = 0;
            //if (!HasNormalBuffer)
            //{
            //    attNormalOffset = numDefaultFloats;
            //    attNormalStride = 0;
            //    numDefaultFloats += 3;
            //}
            //if (!HasTexCoordBuffer)
            //{
            //    attTexCoordOffset = numDefaultFloats;
            //    attTexCoordStride = 0;
            //    numDefaultFloats += 2;
            //}
            //if (!HasColorBuffer)
            //{
            //    attColorOffset = numDefaultFloats;
            //    attColorStride = 0;
            //    numDefaultFloats += 4;
            //}
            //if (!HasTangentBuffer)
            //{
            //    attTangentOffset = numDefaultFloats;
            //    attTangentStride = 0;
            //    numDefaultFloats += 3;
            //}
            vertexOffset = numDefaultFloats;
            vertBufferSize = numDefaultFloats + vertexStride * NumVertices;

            attPositionOffset = numDefaultFloats;
            attPositionStride = vertexStride;

            //just a temporary variable to make assigning variables easier
            int currentAttOffset = numDefaultFloats + 3; //3 since we already have 3 floats for position in each vertex
            //if (HasNormalBuffer)
            //{
            attNormalOffset = currentAttOffset;
            attNormalStride = vertexStride;
            currentAttOffset += 3;
            //}
            //if (HasTexCoordBuffer)
            //{
            attTexCoordOffset = currentAttOffset;
            attTexCoordStride = vertexStride;
            currentAttOffset += 2;
            //}
            //if (HasColorBuffer)
            //{
            attColorOffset = currentAttOffset;
            attColorStride = vertexStride;
            currentAttOffset += 4;
            //}
            //if (HasTangentBuffer)
            //{
            attTangentOffset = currentAttOffset;
            attTangentStride = vertexStride;
            currentAttOffset += 3;
            //}

            //Now we combine all the value's attributes and position data into one array to pass to OpenGL
            //The order is Position, Color, Texture Coordinate, and Normal
            float[] vertexBuffer = new float[vertBufferSize];

            //{//We fill the default values space reserved at the beginning
            //    int currentDefIndex = 0;
            //    if (!HasNormalBuffer)
            //    {
            //        vertexBuffer[currentDefIndex + 0] = defaultNormal.X;
            //        vertexBuffer[currentDefIndex + 1] = defaultNormal.Y;
            //        vertexBuffer[currentDefIndex + 2] = defaultNormal.Z;
            //        currentDefIndex += 3;
            //    }
            //    if (!HasTexCoordBuffer)
            //    {
            //        vertexBuffer[currentDefIndex + 0] = defaultTexCoord.X;
            //        vertexBuffer[currentDefIndex + 1] = defaultTexCoord.Y;
            //        currentDefIndex += 2;
            //    }
            //    if (!HasColorBuffer)
            //    {
            //        vertexBuffer[currentDefIndex + 0] = defaultColor.X;
            //        vertexBuffer[currentDefIndex + 1] = defaultColor.Y;
            //        vertexBuffer[currentDefIndex + 2] = defaultColor.Z;
            //        vertexBuffer[currentDefIndex + 3] = defaultColor.W;
            //        currentDefIndex += 4;
            //    }
            //    if (!HasTangentBuffer)
            //    {
            //        vertexBuffer[currentDefIndex + 0] = defaultTangent.X;
            //        vertexBuffer[currentDefIndex + 1] = defaultTangent.Y;
            //        vertexBuffer[currentDefIndex + 2] = defaultTangent.Z;
            //        currentDefIndex += 3;
            //    }
            //}

            for (int i = 0; i < NumVertices; i++)
            {
                int currentOffset = i * vertexStride + numDefaultFloats;
                vertexBuffer[currentOffset] = vertices[i].X; currentOffset++;
                vertexBuffer[currentOffset] = vertices[i].Y; currentOffset++;
                vertexBuffer[currentOffset] = vertices[i].Z; currentOffset++;

                if (HasNormalBuffer)
                {
                    vertexBuffer[currentOffset] = normals[i].X; currentOffset++;
                    vertexBuffer[currentOffset] = normals[i].Y; currentOffset++;
                    vertexBuffer[currentOffset] = normals[i].Z; currentOffset++;
                }
                else
                {
                    vertexBuffer[currentOffset] = defaultNormal.X; currentOffset++;
                    vertexBuffer[currentOffset] = defaultNormal.Y; currentOffset++;
                    vertexBuffer[currentOffset] = defaultNormal.Z; currentOffset++;
                }

                if (HasTexCoordBuffer)
                {
                    vertexBuffer[currentOffset] = texCoords[i].X; currentOffset++;
                    vertexBuffer[currentOffset] = texCoords[i].Y; currentOffset++;
                }
                else
                {
                    vertexBuffer[currentOffset] = defaultTexCoord.X; currentOffset++;
                    vertexBuffer[currentOffset] = defaultTexCoord.Y; currentOffset++;
                }

                if (HasColorBuffer)
                {
                    vertexBuffer[currentOffset] = colors[i].X; currentOffset++;
                    vertexBuffer[currentOffset] = colors[i].Y; currentOffset++;
                    vertexBuffer[currentOffset] = colors[i].Z; currentOffset++;
                    vertexBuffer[currentOffset] = colors[i].W; currentOffset++;
                }
                else
                {
                    vertexBuffer[currentOffset] = defaultColor.X; currentOffset++;
                    vertexBuffer[currentOffset] = defaultColor.Y; currentOffset++;
                    vertexBuffer[currentOffset] = defaultColor.Z; currentOffset++;
                    vertexBuffer[currentOffset] = defaultColor.W; currentOffset++;
                }

                if (HasTangentBuffer)
                {
                    vertexBuffer[currentOffset] = tangents[i].X; currentOffset++;
                    vertexBuffer[currentOffset] = tangents[i].Y; currentOffset++;
                    vertexBuffer[currentOffset] = tangents[i].Z; currentOffset++;
                }
                else
                {
                    vertexBuffer[currentOffset] = defaultTangent.X; currentOffset++;
                    vertexBuffer[currentOffset] = defaultTangent.Y; currentOffset++;
                    vertexBuffer[currentOffset] = defaultTangent.Z; currentOffset++;
                }
            }

            //Bind it so we can work on it
            GL.BindBuffer(BufferTarget.ArrayBuffer, vertBufferID);
            GL.BufferData<float>(BufferTarget.ArrayBuffer, //We just bound it to ArrayBuffer
                (IntPtr)(vertBufferSize * sizeof(float)), //Size of the whole buffer in bytes
                vertexBuffer, //A reference to the actual data
                usageHint);//Tell OpenGL how/when we are going to use this buffer

            if (HasIndexBuffer)
            {
                //Do the same thing for the index buffer if we are using one
                if (indBufferID == -1)
                    indBufferID = GL.GenBuffer();

                GL.BindBuffer(BufferTarget.ElementArrayBuffer, indBufferID);
                GL.BufferData<uint>(BufferTarget.ElementArrayBuffer,
                    (IntPtr)(sizeof(uint) * indices.Length),
                    indices,
                    usageHint);
            }

            this.buffersCreated = true;
        }


        /// <summary>
        /// Returns the offset of the position attribute. (in num of floats)
        /// </summary>
        public int GetPositionOffset() { return attPositionOffset; }
        /// <summary>
        /// Returns the stride of the position attribute. (in num floats)
        /// </summary>
        public int GetPositionStride() { return attPositionStride; }

        /// <summary>
        /// Returns the offset of the normal attribute. (in num of floats)
        /// </summary>
        public int GetNormalOffset() { return attNormalOffset; }
        /// <summary>
        /// Returns the stride of the normal attribute. (in num floats)
        /// </summary>
        public int GetNormalStride() { return attNormalStride; }

        /// <summary>
        /// Returns the offset of the texture coordinate attribute. (in num of floats)
        /// </summary>
        public int GetTexCoordOffset() { return attTexCoordOffset; }
        /// <summary>
        /// Returns the stride of the texture attribute. (in num floats)
        /// </summary>
        public int GetTexCoordStride() { return attTexCoordStride; }

        /// <summary>
        /// Returns the offset of the color attribute. (in num of floats)
        /// </summary>
        public int GetColorOffset() { return attColorOffset; }
        /// <summary>
        /// Returns the stride of the color attribute. (in num floats)
        /// </summary>
        public int GetColorStride() { return attColorStride; }

        /// <summary>
        /// Returns the offset of the tangent attribute. (in num of floats)
        /// </summary>
        public int GetTangentOffset() { return attTangentOffset; }
        /// <summary>
        /// Returns the stride of the tangent attribute. (in num floats)
        /// </summary>
        public int GetTangentStride() { return attTangentStride; }


        #region Public Static Shape Creators
        /// <summary>
        /// Creates a DrawableObject that represents a 1x1 size cube made of quads with normals and indices
        /// See Notes.txt for vertex order
        /// Note: Does not call DrawableObject.CreateBuffers()
        /// </summary>
        /// <param name="centered">If true the cube will be centered around (0, 0, 0)</param>
        /// <param name="hasTexCoordBuffer">If true it will create texture coordinates for each side</param>
        /// <param name="hasColorBuffer">If true it will generate a color buffer filled with white</param>
        public static DrawableObject GetUnitCube(bool centered = true, bool hasTexCoordBuffer = true, bool hasColorBuffer = false, bool hasTangentBuffer = false)
        {
            DrawableObject obj = new DrawableObject(PrimitiveType.Quads, false, true, hasTexCoordBuffer, hasColorBuffer, hasTangentBuffer);
            AABB cube = new AABB(
                (centered ? new Vector3(-0.5f, -0.5f, -0.5f) : Vector3.Zero),
                (centered ? new Vector3(0.5f, 0.5f, 0.5f) : new Vector3(1, 1, 1)));

            obj.vertices = new Vector3[24];

            //Top Face
            obj.vertices[0] = cube.TopLeftFront;
            obj.vertices[1] = cube.TopRightFront;
            obj.vertices[2] = cube.TopRightBack;
            obj.vertices[3] = cube.TopLeftBack;
            //Bottom Face
            obj.vertices[4] = cube.BottomLeftBack;
            obj.vertices[5] = cube.BottomRightBack;
            obj.vertices[6] = cube.BottomRightFront;
            obj.vertices[7] = cube.BottomLeftFront;
            //Front Face
            obj.vertices[8] = cube.TopRightFront;
            obj.vertices[9] = cube.TopLeftFront;
            obj.vertices[10] = cube.BottomLeftFront;
            obj.vertices[11] = cube.BottomRightFront;
            //Left Face
            obj.vertices[12] = cube.TopLeftFront;
            obj.vertices[13] = cube.TopLeftBack;
            obj.vertices[14] = cube.BottomLeftBack;
            obj.vertices[15] = cube.BottomLeftFront;
            //Back Face
            obj.vertices[16] = cube.TopLeftBack;
            obj.vertices[17] = cube.TopRightBack;
            obj.vertices[18] = cube.BottomRightBack;
            obj.vertices[19] = cube.BottomLeftBack;
            //Right Face
            obj.vertices[20] = cube.TopRightBack;
            obj.vertices[21] = cube.TopRightFront;
            obj.vertices[22] = cube.BottomRightFront;
            obj.vertices[23] = cube.BottomRightBack;

            //Normals
            obj.normals = new Vector3[24];
            for (int i = 0; i < 6; i++)//For each side
            {
                for (int i2 = 0; i2 < 4; i2++)//For each of 4 vertices on that side
                {
                    switch (i)//Normal changes depending on which side we are on
                    {
                        case 0://Top Face
                            obj.normals[i * 4 + i2] = new Vector3(0, 1, 0);
                            break;
                        case 1://Bottom Face
                            obj.normals[i * 4 + i2] = new Vector3(0, -1, 0);
                            break;
                        case 2://Front Face
                            obj.normals[i * 4 + i2] = new Vector3(0, 0, -1);
                            break;
                        case 3://Left Face
                            obj.normals[i * 4 + i2] = new Vector3(-1, 0, 0);
                            break;
                        case 4://Back Face
                            obj.normals[i * 4 + i2] = new Vector3(0, 0, 1);
                            break;
                        case 5://Right Face
                            obj.normals[i * 4 + i2] = new Vector3(1, 0, 0);
                            break;
                    };
                }
            }

            if (hasTangentBuffer)
            {
                obj.tangents = new Vector3[24];
                for (int i = 0; i < 6; i++)//For each side
                {
                    for (int i2 = 0; i2 < 4; i2++)//For each of 4 vertices on that side
                    {
                        switch (i)//Normal changes depending on which side we are on
                        {
                            case 0://Top Face
                                obj.tangents[i * 4 + i2] = new Vector3(1, 0, 0);
                                break;
                            case 1://Bottom Face
                                obj.tangents[i * 4 + i2] = new Vector3(1, 0, 0);
                                break;
                            case 2://Front Face
                                obj.tangents[i * 4 + i2] = new Vector3(-1, 0, 0);
                                break;
                            case 3://Left Face
                                obj.tangents[i * 4 + i2] = new Vector3(0, 0, 1);
                                break;
                            case 4://Back Face
                                obj.tangents[i * 4 + i2] = new Vector3(1, 0, 0);
                                break;
                            case 5://Right Face
                                obj.tangents[i * 4 + i2] = new Vector3(0, 0, -1);
                                break;
                        };
                    }
                }
            }

            if (hasColorBuffer)
            {
                obj.colors = new Vector4[24];
                for (int i = 0; i < 24; i++)
                    obj.colors[i] = Calc.FromColor(Color.White);
            }
            if (hasTexCoordBuffer)
            {
                obj.texCoords = new Vector2[24];
                for (int i = 0; i < 6; i++)
                {
                    obj.texCoords[i * 4 + 0] = new Vector2(0, 0);
                    obj.texCoords[i * 4 + 1] = new Vector2(1, 0);
                    obj.texCoords[i * 4 + 2] = new Vector2(1, 1);
                    obj.texCoords[i * 4 + 3] = new Vector2(0, 1);
                }
            }

            return obj;
        }

        /// <summary>
        /// Creates a heightmap made of triangles from an image's color data. Uses Color.
        /// Note: Does not call DrawableObject.CreateBuffers()
        /// </summary>
        /// <param name="heightmapData"></param>
        /// <param name="size"></param>
        /// <param name="minHeight"></param>
        /// <param name="maxHeight"></param>
        /// <param name="numTextureRepetitions">The number of times the texture repeats (x and y axis) along the whole heightmap</param>
        /// <returns></returns>
        public static DrawableObject CreateHeightmap(Texture2D heightmapImage, Vector2 size, float minHeight, float maxHeight, int numTextureRepetitions)
        {
            GL.BindTexture(TextureTarget.Texture2D, heightmapImage.Id);
            float[] imageData = new float[heightmapImage.Width * heightmapImage.Height * 3];
            GL.GetTexImage(TextureTarget.Texture2D, 0, PixelFormat.Rgb, PixelType.Float, imageData);

            DrawableObject heightmap = new DrawableObject(PrimitiveType.Triangles, true, true, true, false, false);
            heightmap.vertices = new Vector3[heightmapImage.Width * heightmapImage.Height];
            heightmap.texCoords = new Vector2[heightmapImage.Width * heightmapImage.Height];
            for (int x = 0; x < heightmapImage.Width; x++)
            {
                for (int y = 0; y < heightmapImage.Height; y++)
                {
                    int index = x + y * heightmapImage.Width;
                    Color currentColor = Color.FromArgb(
                        (byte)255,
                        (byte)Calc.Lerp(0, 255, imageData[index * 3 + 0]),
                        (byte)Calc.Lerp(0, 255, imageData[index * 3 + 1]),
                        (byte)Calc.Lerp(0, 255, imageData[index * 3 + 2]));
                    heightmap.vertices[index] = new Vector3(
                        x * (size.X / heightmapImage.Width),
                        Calc.Lerp(minHeight, maxHeight, currentColor.GetBrightness()),
                        y * (size.Y / heightmapImage.Height));
                    heightmap.texCoords[index] = new Vector2(
                        (float)x / heightmapImage.Width * numTextureRepetitions,
                        (float)y / heightmapImage.Height * numTextureRepetitions);
                }
            }

            heightmap.indices = new uint[heightmapImage.Width * heightmapImage.Height * 6];
            for (int x = 0; x < heightmapImage.Width - 1; x++)
            {
                for (int y = 0; y < heightmapImage.Height - 1; y++)
                {
                    int index = (x + y * heightmapImage.Width) * 6;

                    heightmap.indices[index + 0] = (uint)((x + 0) + (y + 0) * heightmapImage.Width);
                    heightmap.indices[index + 1] = (uint)((x + 1) + (y + 0) * heightmapImage.Width);
                    heightmap.indices[index + 2] = (uint)((x + 1) + (y + 1) * heightmapImage.Width);

                    heightmap.indices[index + 3] = (uint)((x + 0) + (y + 0) * heightmapImage.Width);
                    heightmap.indices[index + 4] = (uint)((x + 1) + (y + 1) * heightmapImage.Width);
                    heightmap.indices[index + 5] = (uint)((x + 0) + (y + 1) * heightmapImage.Width);
                }
            }

            return heightmap;
        }

        /// <summary>
        /// Creates a sphere with radius of 1 out of a specified number of rings and points on those rings.
        /// The drawable object is a triangle list.
        /// </summary>
        /// <param name="ringDivisions">The number of points along one ring</param>
        /// <param name="numRings">The number of rings (not including the top and bottom points)</param>
        /// <param name="centered">Wether or not to put the center at (0,0,0). if not then at (0.5, 0.5, 0.5)</param>
        /// <param name="hasTexCoordBuffer">Whether or not to create a texture buffer in the drawable object</param>
        /// <param name="hasColorBuffer">Whether or not to create a color buffer in the drawable object</param>
        /// <returns>A drawable object with all of the data to represent a unit sphere. (CreateBuffers is not called)</returns>
        public static DrawableObject GetUnitSphere(int ringDivisions, int numRings, bool centered = true, bool hasTexCoordBuffer = true, bool hasColorBuffer = true)
        {
            DrawableObject obj = new DrawableObject(PrimitiveType.Triangles, true, true, hasColorBuffer, hasTexCoordBuffer, false);

            Vector3 center = centered ? Vector3.Zero : new Vector3(0.5f);
            float radius = 1f;

            obj.vertices = new Vector3[numRings * ringDivisions + 2];//2 is for top and bottom points
            obj.normals = new Vector3[obj.vertices.Length];
            if (hasTexCoordBuffer)
                obj.texCoords = new Vector2[obj.vertices.Length];
            if (hasColorBuffer)
                obj.colors = new Vector4[obj.vertices.Length];

            int numTriangles = 0;
            numTriangles += ringDivisions * 2;//for top and bottom caps
            numTriangles += (numRings - 1) * ringDivisions * 2;//-1 because e.g. if 1 ring then we have only caps and no center triangles
            obj.indices = new uint[numTriangles * 3];

            //top vertex
            obj.vertices[0] = new Vector3(center.X, center.Y + radius, center.Z);
            obj.normals[0] = new Vector3(0, 1, 0);
            if (hasTexCoordBuffer)
                obj.texCoords[0] = Vector2.Zero;
            if (hasColorBuffer)
                obj.colors[0] = Calc.FromColor(Color.White);

            //bottom vertex
            obj.vertices[obj.vertices.Length - 1] = new Vector3(center.X, center.Y - radius, center.Z);
            obj.normals[obj.vertices.Length - 1] = new Vector3(0, 1, 0);
            if (hasTexCoordBuffer)
                obj.texCoords[obj.vertices.Length - 1] = Vector2.Zero;
            if (hasColorBuffer)
                obj.colors[obj.vertices.Length - 1] = Calc.FromColor(Color.White);

            int currentIndex = 1;//the index of the last instantiated vertex
            int currentTriangle = 0;//the index of what triangle we are going to create indicies for
            float angleDelta = -MathHelper.Pi * 2 / ringDivisions;
            for (int r = 0; r < numRings; r++)
            {
                float addY = radius - (radius * 2) / (numRings + 1) * (r + 1);
                float angleToHeight = (float)Math.Acos(addY / radius);
                float ringRadius = (float)Math.Sin(angleToHeight) * radius;
                for (int i = 0; i < ringDivisions; i++)
                {
                    obj.vertices[currentIndex] = new Vector3(
                        center.X + (float)Math.Cos(angleDelta * i) * ringRadius,
                        center.Y + addY,
                        center.Z + (float)Math.Sin(angleDelta * i) * ringRadius);
                    obj.normals[currentIndex] = Vector3.Normalize(obj.vertices[currentIndex] - center);

                    if (hasTexCoordBuffer)
                        obj.texCoords[currentIndex] = new Vector2(0, 0);
                    if (hasColorBuffer)
                        obj.colors[currentIndex] = Calc.FromColor(Color.White);

                    #region Create Indicies
                    if (i != 0)//Can't create a quad/triangle from the first vertex of a ring
                    {
                        if (r == 0)
                        {
                            //Create triangle for top cap
                            obj.indices[currentTriangle * 3 + 0] = 0;
                            obj.indices[currentTriangle * 3 + 1] = (uint)currentIndex;
                            obj.indices[currentTriangle * 3 + 2] = (uint)currentIndex - 1;
                            currentTriangle++;

                            if (i == ringDivisions - 1)
                            {
                                //add last triangle to complete cap
                                obj.indices[currentTriangle * 3 + 0] = 0;
                                obj.indices[currentTriangle * 3 + 1] = (uint)(currentIndex - ringDivisions + 1);//connect to first of this ring
                                obj.indices[currentTriangle * 3 + 2] = (uint)currentIndex;
                                currentTriangle++;
                            }
                        }
                        if (r == numRings - 1)
                        {
                            //Create triangle for bottom cap
                            obj.indices[currentTriangle * 3 + 0] = (uint)obj.vertices.Length - 1;
                            obj.indices[currentTriangle * 3 + 1] = (uint)currentIndex - 1;
                            obj.indices[currentTriangle * 3 + 2] = (uint)currentIndex;
                            currentTriangle++;

                            if (i == ringDivisions - 1)
                            {
                                //add last triangle to complete cap
                                obj.indices[currentTriangle * 3 + 0] = (uint)obj.vertices.Length - 1;
                                obj.indices[currentTriangle * 3 + 1] = (uint)currentIndex;
                                obj.indices[currentTriangle * 3 + 2] = (uint)(currentIndex - ringDivisions + 1);//connect to first of this ring
                                currentTriangle++;
                            }
                        }
                        if (r != 0)
                        {
                            //Create quad for middle ring
                            obj.indices[currentTriangle * 3 + 0] = (uint)(currentIndex - ringDivisions - 1);//Up a ring and left one
                            obj.indices[currentTriangle * 3 + 1] = (uint)(currentIndex);
                            obj.indices[currentTriangle * 3 + 2] = (uint)(currentIndex - 1);//left a vertex
                            currentTriangle++;

                            obj.indices[currentTriangle * 3 + 0] = (uint)(currentIndex - ringDivisions - 1);//Up a ring and left one
                            obj.indices[currentTriangle * 3 + 1] = (uint)(currentIndex - ringDivisions);//just up a ring
                            obj.indices[currentTriangle * 3 + 2] = (uint)(currentIndex);
                            currentTriangle++;

                            if (i == ringDivisions - 1)
                            {
                                //Create last quad to finish a ring
                                obj.indices[currentTriangle * 3 + 0] = (uint)(currentIndex - ringDivisions);//Up a ring
                                obj.indices[currentTriangle * 3 + 1] = (uint)(currentIndex - ringDivisions + 1);//first vertex of this ring
                                obj.indices[currentTriangle * 3 + 2] = (uint)(currentIndex);
                                currentTriangle++;

                                obj.indices[currentTriangle * 3 + 0] = (uint)(currentIndex - ringDivisions);//Up a ring
                                obj.indices[currentTriangle * 3 + 1] = (uint)(currentIndex - ringDivisions * 2 + 1);//first vertex of upper ring
                                obj.indices[currentTriangle * 3 + 2] = (uint)(currentIndex - ringDivisions + 1);//first vertex of this ring
                                currentTriangle++;

                            }
                        }
                    }
                    #endregion

                    currentIndex++;
                }
            }

            return obj;
        }
        #endregion
    }
}
