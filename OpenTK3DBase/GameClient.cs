﻿using OpenTK;
using System.Drawing;
using OpenTK.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK.Graphics.OpenGL;

namespace OpenTK3DBase
{
    /// <summary>
    /// The GameClient class is where all your in game code should be done.
    /// If another game state besides "in-game" is added, you should make a different client class
    /// and work with the Game1.cs to handle switching between clients
    /// </summary>
    class GameClient
    {
        /// <summary>
        /// The view object for our game client
        /// </summary>
        public View view;
        /// <summary>
        /// A reference to the Game1 object created in Program.cs
        /// </summary>
        public Game1 game1;

        DrawableObject sphere, obj;
        Texture2D texture;

        /// <summary>
        /// Basic constructor, doesn't do anything besides save a reference to game1
        /// </summary>
        /// <param name="game1">A reference to the game1 object created in Program.cs</param>
        public GameClient(Game1 game1)
        {
            //Make sure you don't do anything here that
            //should be done in Initialize
            this.game1 = game1;
        }

        /// <summary>
        /// Called by Game1 before Initialize
        /// </summary>
        public void LoadContent()
        {
            texture = ContentPipe.LoadTexture("Textures/Checker10.jpg", false);
        }
        /// <summary>
        /// This is our initialize function. It is called when a GameClient class is getting ready to run. 
        /// LoadContent is called just before.
        /// </summary>
        public void Initialize()
        {
            view = new View(new Vector3(3, 3, 3), new Vector3(0, 0, 0), new Vector3(0, 1, 0));

           // sphere = DrawableObject.GetUnitSphere(30, 8, centered: true, hasTexCoordBuffer: false, hasColorBuffer: false);
            sphere = DrawableObject.GetUnitCube(true, false, false, false);
            sphere.defaultColor = Calc.FromColor(Color.White);
            sphere.CreateBuffers();

            obj = new DrawableObject(PrimitiveType.Triangles, BufferType.TexCoord);
            obj.vertices = new Vector3[]
                {
                    new Vector3(0, 0, 0),
                    new Vector3(5, 0, 5),
                    new Vector3(0, 0, 5),

                    new Vector3(0, 0, 0),
                    new Vector3(5, 0, 0),
                    new Vector3(5, 0, 5),
                };
            obj.texCoords = new Vector2[]
                {
                    new Vector2(0, 0),
                    new Vector2(1, 1),
                    new Vector2(0, 1),

                    new Vector2(0, 0),
                    new Vector2(1, 0),
                    new Vector2(1, 1),
                };
            obj.defaultTexCoord = new Vector2(0, 0);
            obj.defaultColor = Calc.FromColor(Color.White);
            obj.defaultNormal = new Vector3(0, 1, 0);
            obj.defaultTangent = new Vector3(1, 0, 0);
            obj.CreateBuffers();
        }

        /// <param name="elapsedMils">Milliseconds elapsed since last Update</param>
        public void Update(double elapsedMils)
        {
            if (My.window.Focused)
            {
                float dist = Calc.Oscillation(4, 5, 25);
                float rot = Calc.Oscillation(0, MathHelper.Pi * 2, 15);
                view.Position = new Vector3((float)Math.Cos(rot) * dist, 3, (float)Math.Sin(rot) * dist);

                view.Update();
            }
        }
        
        /// <param name="elapsedMils">Milliseconds elapsed since last Render</param>
        public void Draw(double elapsedMils)
        {
            view.ApplyTransforms();

            Renderer.DefaultSettings();

            //Renderer.Draw(sphere, Matrix4.CreateScale(2f), texture, view);
            Renderer.Draw(obj, Matrix4.CreateScale(1f), texture, view);
        }
        
        public void Dispose()
        {

        }
    }
}
