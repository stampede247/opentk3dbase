﻿using OpenTK;
using OpenTK.Graphics.OpenGL;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenTK3DBase
{
    public class VoxelBufferFactory
    {
        /// <summary>
        /// Takes a single VoxelObject and fills it's 'buffer' value with a new DrawableObject.
        /// Calls obj.buffer.CreateBuffers()
        /// </summary>
        /// <param name="obj">the Voxel Object to generate for</param>
        /// <param name="showEdgeSides">Whether or not to generate sides for faces at the edge of the voxel object's array bounderies</param>
        /// <param name="usageHint">To pass to obj.buffer.CreateBuffers()</param>
        public static void GenerateBuffersFor(ref VoxelObject obj, bool showEdgeSides = true, BufferUsageHint usageHint = BufferUsageHint.StaticDraw)
        {
            List<Vector3> vertices = new List<Vector3>();
            List<Vector4> colors = new List<Vector4>();
            List<Vector2> texCoords = new List<Vector2>();
            List<Vector3> normals = new List<Vector3>();
            List<Vector3> tangents = new List<Vector3>();
            List<uint> indices = new List<uint>();

            #region Fill Lists With Vertex Data
            for (int x = 0; x < obj.Width; x++)
            {
                for (int y = 0; y < obj.Height; y++)
                {
                    for (int z = 0; z < obj.Length; z++)
                    {
                        //These represent the adjacent voxels to the current one in each of 6 directions
                        Voxel leftVoxel = (x > 0) ? obj.voxels[x - 1, y, z] : (showEdgeSides ? Voxel.Empty : Voxel.Solid);
                        Voxel rightVoxel = (x < obj.Width - 1) ? obj.voxels[x + 1, y, z] : (showEdgeSides ? Voxel.Empty : Voxel.Solid);
                        Voxel bottomVoxel = (y > 0) ? obj.voxels[x, y - 1, z] : (showEdgeSides ? Voxel.Empty : Voxel.Solid);
                        Voxel topVoxel = (y < obj.Height - 1) ? obj.voxels[x, y + 1, z] : (showEdgeSides ? Voxel.Empty : Voxel.Solid);
                        Voxel frontVoxel = (z > 0) ? obj.voxels[x, y, z - 1] : (showEdgeSides ? Voxel.Empty : Voxel.Solid);
                        Voxel backVoxel = (z < obj.Length - 1) ? obj.voxels[x, y, z + 1] : (showEdgeSides ? Voxel.Empty : Voxel.Solid);

                        //Represents the current voxels space
                        AABB curBox = new AABB(x, y, z, 1, 1, 1);

                        //Create all the full faces first
                        //Front Face
                        if (obj.voxels[x, y, z].FullFaceFront && !frontVoxel.FullFaceBack)
                        {
                            vertices.Add(curBox.TopRightFront);
                            vertices.Add(curBox.TopLeftFront);
                            vertices.Add(curBox.BottomLeftFront);
                            vertices.Add(curBox.BottomRightFront);
                            for (int i = 0; i < 4; i++)
                            {
                                normals.Add(new Vector3(0, 0, -1));
                                tangents.Add(new Vector3(-1, 0, 0));
                                colors.Add(Calc.FromColor(obj.voxels[x, y, z].color));
                            }
                            texCoords.Add(new Vector2(0, 0));
                            texCoords.Add(new Vector2(1, 0));
                            texCoords.Add(new Vector2(1, 1));
                            texCoords.Add(new Vector2(0, 1));
                        }
                        //Back Face
                        if (obj.voxels[x, y, z].FullFaceBack && !backVoxel.FullFaceFront)
                        {
                            vertices.Add(curBox.TopLeftBack);
                            vertices.Add(curBox.TopRightBack);
                            vertices.Add(curBox.BottomRightBack);
                            vertices.Add(curBox.BottomLeftBack);
                            for (int i = 0; i < 4; i++)
                            {
                                normals.Add(new Vector3(0, 0, 1));
                                tangents.Add(new Vector3(1, 0, 0));
                                colors.Add(Calc.FromColor(obj.voxels[x, y, z].color));
                            }
                            texCoords.Add(new Vector2(0, 0));
                            texCoords.Add(new Vector2(1, 0));
                            texCoords.Add(new Vector2(1, 1));
                            texCoords.Add(new Vector2(0, 1));
                        }
                        //Left Face
                        if (obj.voxels[x, y, z].FullFaceLeft && !leftVoxel.FullFaceRight)
                        {
                            vertices.Add(curBox.TopLeftFront);
                            vertices.Add(curBox.TopLeftBack);
                            vertices.Add(curBox.BottomLeftBack);
                            vertices.Add(curBox.BottomLeftFront);
                            for (int i = 0; i < 4; i++)
                            {
                                normals.Add(new Vector3(-1, 0, 0));
                                tangents.Add(new Vector3(0, 0, 1));
                                colors.Add(Calc.FromColor(obj.voxels[x, y, z].color));
                            }
                            texCoords.Add(new Vector2(0, 0));
                            texCoords.Add(new Vector2(1, 0));
                            texCoords.Add(new Vector2(1, 1));
                            texCoords.Add(new Vector2(0, 1));
                        }
                        //Right Face
                        if (obj.voxels[x, y, z].FullFaceRight && !rightVoxel.FullFaceLeft)
                        {
                            vertices.Add(curBox.TopRightBack);
                            vertices.Add(curBox.TopRightFront);
                            vertices.Add(curBox.BottomRightFront);
                            vertices.Add(curBox.BottomRightBack);
                            for (int i = 0; i < 4; i++)
                            {
                                normals.Add(new Vector3(1, 0, 0));
                                tangents.Add(new Vector3(0, 0, -1));
                                colors.Add(Calc.FromColor(obj.voxels[x, y, z].color));
                            }
                            texCoords.Add(new Vector2(0, 0));
                            texCoords.Add(new Vector2(1, 0));
                            texCoords.Add(new Vector2(1, 1));
                            texCoords.Add(new Vector2(0, 1));
                        }
                        //Top Face
                        if (obj.voxels[x, y, z].FullFaceTop && !topVoxel.FullFaceBottom)
                        {
                            vertices.Add(curBox.TopLeftFront);
                            vertices.Add(curBox.TopRightFront);
                            vertices.Add(curBox.TopRightBack);
                            vertices.Add(curBox.TopLeftBack);
                            for (int i = 0; i < 4; i++)
                            {
                                normals.Add(new Vector3(0, 1, 0));
                                tangents.Add(new Vector3(1, 0, 0));
                                colors.Add(Calc.FromColor(obj.voxels[x, y, z].color));
                            }
                            texCoords.Add(new Vector2(0, 0));
                            texCoords.Add(new Vector2(1, 0));
                            texCoords.Add(new Vector2(1, 1));
                            texCoords.Add(new Vector2(0, 1));
                        }
                        //Bottom Face
                        if (obj.voxels[x, y, z].FullFaceBottom && !bottomVoxel.FullFaceTop)
                        {
                            vertices.Add(curBox.BottomLeftBack);
                            vertices.Add(curBox.BottomRightBack);
                            vertices.Add(curBox.BottomRightFront);
                            vertices.Add(curBox.BottomLeftFront);
                            for (int i = 0; i < 4; i++)
                            {
                                normals.Add(new Vector3(0, -1, 0));
                                tangents.Add(new Vector3(1, 0, 0));
                                colors.Add(Calc.FromColor(obj.voxels[x, y, z].color));
                            }
                            texCoords.Add(new Vector2(0, 0));
                            texCoords.Add(new Vector2(1, 0));
                            texCoords.Add(new Vector2(1, 1));
                            texCoords.Add(new Vector2(0, 1));
                        }
                    }
                }
            }

            for (int i = 0; i < vertices.Count; i++)
            {
                indices.Add((uint)i);
            }
            #endregion

            if (obj.buffer == null)
                obj.buffer = new DrawableObject(PrimitiveType.Quads, true, true, true, true, true);

            obj.buffer.vertices = vertices.ToArray();
            obj.buffer.colors = colors.ToArray();
            obj.buffer.texCoords = texCoords.ToArray();
            obj.buffer.normals = normals.ToArray();
            obj.buffer.indices = indices.ToArray();
            obj.buffer.tangents = tangents.ToArray();

            obj.buffer.CreateBuffers(usageHint);
        }
    }
}
