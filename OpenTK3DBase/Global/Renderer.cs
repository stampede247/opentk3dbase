﻿using OpenTK;
using OpenTK.Graphics.OpenGL;
using System.Drawing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenTK3DBase
{
    public class Renderer
    {
        /// <summary>
        /// If this is initialized and no shader is passed any of the draw functions
        /// then we will use this shader to draw objects. Otherwise we will use no 
        /// custom shader program and simply let OpenGL do default functionality
        /// </summary>
        public static Shader defaultShader;
        /// <summary>
        /// A reference to the view that should be used if no view is defined when calling the Draw function
        /// </summary>
        public static View defaultView;
        /// <summary>
        /// A reference to the SurfaceMaterial that should be used if no material is defined when calling the Draw function
        /// </summary>
        public static SurfaceMaterial? defaultMaterial;
        /// <summary>
        /// A reference to the Texture2D that should be used if no material is defined when calling the Draw function
        /// </summary>
        public static Texture2D? defaultTexture;

        /// <summary>
        /// Sets all the default settings on OpenGL for drawing
        /// regular 3D objects
        /// </summary>
        public static void DefaultSettings()
        {
            GL.Enable(EnableCap.Texture2D);
            GL.Enable(EnableCap.Blend);
            GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);
            GL.Enable(EnableCap.DepthTest);
            GL.DepthFunc(DepthFunction.Lequal);
            GL.Disable(EnableCap.CullFace);
            GL.CullFace(CullFaceMode.Front);
        }

        /// <summary>
        /// Draws the DrawableObject using the transformMatrix and shader.
        /// </summary>
        /// <param name="drawable">The object to be drawn. CreateBuffers() should already have been called</param>
        /// <param name="transformMatrix">A Matrix4 defining the transformations to be applied before drawing the object</param>
        /// <param name="shader">The Shader program to use. If not given or null then we will use this.defaultShader if it has a value. 
        /// Otherwise we will draw using the default OpenGL methods</param>
        public static void Draw(DrawableObject drawable, Matrix4 transformMatrix, Texture2D? texture = null, View view = null, Shader shader = null, SurfaceMaterial? material = null)
        {
            if (!drawable.BuffersCreated)
                throw new ArgumentException("The DrawableObject has not had it's buffers created yet. Call CreateBuffers() before passing it to this draw function.");

            Shader useShader = (shader == null ? defaultShader : shader);//the shader we will use, can be null
            View useView = (view == null ? defaultView : view);//the view we will use, can be null
            SurfaceMaterial? useMaterial = (material.HasValue ? material : defaultMaterial);//the material we will use, can be null
            Texture2D? useTexture = (texture.HasValue ? texture : (Texture2D?)defaultTexture);//the texture we will use, can be null

            if (useShader == null)
            {
                GL.UseProgram(0);
            }
            else
            {
                GL.UseProgram(useShader.Program.ProgramID);
            }

            GL.BindBuffer(BufferTarget.ArrayBuffer, drawable.VertBufferID);

            if (useShader == null)
            {
                GL.EnableClientState(ArrayCap.VertexArray);
                GL.VertexPointer(3, VertexPointerType.Float,
                    drawable.GetPositionStride() * sizeof(float),
                    drawable.GetPositionOffset() * sizeof(float));
                
                GL.EnableClientState(ArrayCap.NormalArray);
                GL.NormalPointer(NormalPointerType.Float,
                    drawable.GetNormalStride() * sizeof(float),
                    drawable.GetNormalOffset() * sizeof(float));

                GL.EnableClientState(ArrayCap.TextureCoordArray);
                GL.TexCoordPointer(2, TexCoordPointerType.Float,
                    drawable.GetTexCoordStride() * sizeof(float),
                    drawable.GetTexCoordOffset() * sizeof(float));

                GL.EnableClientState(ArrayCap.ColorArray);
                GL.ColorPointer(4, ColorPointerType.Float,
                    drawable.GetColorStride() * sizeof(float),
                    drawable.GetColorOffset() * sizeof(float));
                //Tangents are not allowed/ needed in default OpenGL drawing
            }
            else
            {
                useShader.SetPositionPointer(drawable.GetPositionOffset() * sizeof(float), drawable.GetPositionStride() * sizeof(float));
                try { useShader.SetNormalPointer(drawable.GetNormalOffset() * sizeof(float), drawable.GetNormalStride() * sizeof(float)); } catch (NotImplementedException ex) { }
                try { useShader.SetTexCoordPointer(drawable.GetTexCoordOffset() * sizeof(float), drawable.GetTexCoordStride() * sizeof(float)); } catch (NotImplementedException ex) { }
                try { useShader.SetColorPointer(drawable.GetColorOffset() * sizeof(float), drawable.GetColorStride() * sizeof(float)); } catch (NotImplementedException ex) { }
                try { useShader.SetTangentPointer(drawable.GetTangentOffset() * sizeof(float), drawable.GetTangentStride() * sizeof(float)); } catch (NotImplementedException ex) { }
            }

            if (drawable.HasIndexBuffer)
            {
                GL.BindBuffer(BufferTarget.ElementArrayBuffer, drawable.IndBufferID);
            }

            if (useShader != null)
            {
                useShader.Program.EnableVertexAttribArrays();

                if (!useTexture.HasValue)
                    try { useShader.SetTexture(My.dot); } catch (NotImplementedException ex) { }
                else
                    try { useShader.SetTexture(useTexture.Value); } catch (NotImplementedException ex) { }

                if (useView == null)
                {
                    try { useShader.SetWorldMatrix(transformMatrix); } catch (NotImplementedException ex) { }
                    try { useShader.SetModelViewMatrix(Matrix4.Identity); } catch (NotImplementedException ex) { }
                    try { useShader.SetProjectionMatrix(Matrix4.Identity); } catch (NotImplementedException ex) { }
                    useShader.SetCombinedMatrix(transformMatrix);
                }
                else
                {
                    try { useShader.SetWorldMatrix(transformMatrix); } catch (NotImplementedException ex) { }
                    try { useShader.SetModelViewMatrix(useView.ViewMatrix); } catch (NotImplementedException ex) { }
                    try { useShader.SetProjectionMatrix(useView.ProjMatrix); } catch (NotImplementedException ex) { }
                    useShader.SetCombinedMatrix(transformMatrix * useView.ViewMatrix * useView.ProjMatrix);
                }
            }
            else
            {
                if (useTexture.HasValue)
                    GL.BindTexture(TextureTarget.Texture2D, useTexture.Value.Id);
                else
                    GL.BindTexture(TextureTarget.Texture2D, My.dot.Id);

                Matrix4 modelView = transformMatrix * view.ViewMatrix;
                GL.MatrixMode(MatrixMode.Modelview);
                GL.LoadMatrix(ref modelView);

                Matrix4 projection = view.ProjMatrix;
                GL.MatrixMode(MatrixMode.Projection);
                GL.LoadMatrix(ref projection);
            }

            if (drawable.HasIndexBuffer)
            {
                GL.DrawElements(drawable.primitiveType, drawable.NumIndices, DrawElementsType.UnsignedInt, 0);
            }
            else
            {
                GL.DrawArrays(drawable.primitiveType, 0, drawable.NumVertices);
            }

            if (useShader != null)
            {
                useShader.Program.DisableVertexAttribArrays();

                GL.UseProgram(0);
            }
        }
    }
}
