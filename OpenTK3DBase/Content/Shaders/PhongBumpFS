﻿#version 400

struct Light
{
	float isOn;//Is either 0 or 1
	float isInfinite;//Is either 0 or 1
	vec3 position;
	vec4 diffuseColor;
	vec4 specularColor;
	float constantAttenuation, linearAttenuation, quadraticAttenuation;
	float spotCutoff, spotExponent;
	vec3 spotDirection;
};
struct Material
{
	vec4 ambientColor;
	vec4 diffuseColor;
	vec4 specularColor;
	float shininess;
};

uniform sampler2D texture;
uniform sampler2D bumpMap;

uniform vec3 viewPos;

uniform vec4 sceneAmbience;

uniform Light light0;
uniform Light light1;
uniform Light light2;
uniform Light light3;
uniform Light light4;
uniform Light light5;
uniform Light light6;
uniform Light light7;
uniform Material material;

in vec3 fPosition;
in vec4 fColor;
in vec2 fTexCoord;
in vec3 fNormal;
in vec3 fTangent;

out vec4 finalColor;

vec4 GetLightingFrom(in vec3 lightPos,
					 in float lightIsInf,
					 in vec4 lightDifColor,
					 in vec4 lightSpecColor,
					 in float lightConAtt,
					 in float lightLinAtt,
					 in float lightQuadAtt,
					 in float lightSpotCutoff,
					 in float lightSpotExponent,
					 in vec3 lightSpotDirection)
{
	vec3 normal = normalize(fNormal);
	//vec3 tangent = normalize(fTangent);

	//vec3 normColor = normalize(2.0 * texture2D(bumpMap, fTexCoord).xyz - 1.0);
	//normal = normalize(
	//	normColor.x * tangent + 
	//	normColor.y * normal + 
	//	normColor.z * cross(tangent, normal));

	vec3 light_direction = normalize(lightPos - fPosition);
	vec3 view_direction = normalize(viewPos - fPosition);
	float distance = length(lightPos - fPosition);
	float attenuation = 0;
	
	if (lightIsInf == 1.0)
	{
		attenuation = 1.0;
		light_direction = normalize(lightPos);
	}
	else
	{
		attenuation = 1.0 / (
			  lightConAtt
			+ lightLinAtt * distance
			+ lightQuadAtt * distance * distance);

		if (lightSpotCutoff <= 90.0) //spotlight
		{
			float clampedCosine = max(0.0, dot(-light_direction, normalize(lightSpotDirection)));
			if (clampedCosine < cos(radians(lightSpotCutoff)))
			{
				attenuation = 0;//Not inside the spotlight
			}
			else
			{
				attenuation = attenuation * pow(clampedCosine, lightSpotExponent);
			}
		}
	}

	//Ambient light
	vec3 ambient = vec3(material.ambientColor);

	//Diffuse light
	vec3 diffuse = attenuation * max(0.0, dot(normal, light_direction))
			  * vec3(lightDifColor) * vec3(material.diffuseColor);

	//Specular light
	vec3 specular_color = lightSpecColor.xyz * material.specularColor;
	float specular_intensity = max(0.0, dot(reflect(-light_direction, normal), view_direction));
	specular_intensity = attenuation * pow(specular_intensity, material.shininess);
	vec3 specular = specular_intensity * specular_color;
	if (material.shininess == 0)
		specular = vec3(0, 0, 0);

	return vec4(ambient + diffuse + specular, 1.0);
}

void main()
{
	vec4 overallLighting = vec4(0, 0, 0, 1);
	if (light0.isOn == 1)
	{
		overallLighting = overallLighting + GetLightingFrom(light0.position, light0.isInfinite,
										   light0.diffuseColor, light0.specularColor,
										   light0.constantAttenuation, light0.linearAttenuation, light0.quadraticAttenuation,
										   light0.spotCutoff, light0.spotExponent, light0.spotDirection);
	}
	if (light1.isOn == 1)
	{
		overallLighting = overallLighting + GetLightingFrom(light1.position, light1.isInfinite,
										   light1.diffuseColor, light1.specularColor,
										   light1.constantAttenuation, light1.linearAttenuation, light1.quadraticAttenuation,
										   light1.spotCutoff, light1.spotExponent, light1.spotDirection);
	}
	if (light2.isOn == 1)
	{
		overallLighting = overallLighting + GetLightingFrom(light2.position, light2.isInfinite,
										   light2.diffuseColor, light2.specularColor,
										   light2.constantAttenuation, light2.linearAttenuation, light2.quadraticAttenuation,
										   light2.spotCutoff, light2.spotExponent, light2.spotDirection);
	}
	if (light3.isOn == 1)
	{
		overallLighting = overallLighting + GetLightingFrom(light3.position, light3.isInfinite,
										   light3.diffuseColor, light3.specularColor,
										   light3.constantAttenuation, light3.linearAttenuation, light3.quadraticAttenuation,
										   light3.spotCutoff, light3.spotExponent, light3.spotDirection);
	}
	if (light4.isOn == 1)
	{
		overallLighting = overallLighting + GetLightingFrom(light4.position, light4.isInfinite,
										   light4.diffuseColor, light4.specularColor,
										   light4.constantAttenuation, light4.linearAttenuation, light4.quadraticAttenuation,
										   light4.spotCutoff, light4.spotExponent, light4.spotDirection);
	}
	if (light5.isOn == 1)
	{
		overallLighting = overallLighting + GetLightingFrom(light4.position, light5.isInfinite,
										   light4.diffuseColor, light4.specularColor,
										   light4.constantAttenuation, light4.linearAttenuation, light4.quadraticAttenuation,
										   light4.spotCutoff, light4.spotExponent, light4.spotDirection);
	}
	if (light6.isOn == 1)
	{
		overallLighting = overallLighting + GetLightingFrom(light6.position, light6.isInfinite,
										   light6.diffuseColor, light6.specularColor,
										   light6.constantAttenuation, light6.linearAttenuation, light6.quadraticAttenuation,
										   light6.spotCutoff, light6.spotExponent, light6.spotDirection);
	}
	if (light7.isOn == 1)
	{
		overallLighting = overallLighting + GetLightingFrom(light7.position, light7.isInfinite,
										   light7.diffuseColor, light7.specularColor,
										   light7.constantAttenuation, light7.linearAttenuation, light7.quadraticAttenuation,
										   light7.spotCutoff, light7.spotExponent, light7.spotDirection);
	}

	overallLighting += sceneAmbience;
	//overallLighting.x = min(1.0, overallLighting.x);
	//overallLighting.y = min(1.0, overallLighting.y);
	//overallLighting.z = min(1.0, overallLighting.z);
	//overallLighting.w = min(1.0, overallLighting.w);

	finalColor = overallLighting * fColor * texture(texture, fTexCoord);
}