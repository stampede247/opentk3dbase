﻿using OpenTK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenTK3DBase
{
    class Program
    {
        static void Main(string[] args)
        {
            GameWindow window = new GameWindow(Game1.WINDOW_WIDTH, Game1.WINDOW_HEIGHT,
                new OpenTK.Graphics.GraphicsMode(Game1.COLOR_BITS, Game1.DEPTH_BITS, Game1.STENCIL_BITS, Game1.SAMPLE_BITS),
                Game1.WINDOW_NAME, Game1.WINDOW_FLAGS);
            Game1 game = new Game1(window);

            Console.WriteLine("Currently running on OpenGL {0}", OpenTK.Graphics.OpenGL.GL.GetString(OpenTK.Graphics.OpenGL.StringName.Version));//TODO:Add to 2D OpenTK Base

            window.Run(Game1.WINDOW_FRAMERATE);
        }
    }
}
