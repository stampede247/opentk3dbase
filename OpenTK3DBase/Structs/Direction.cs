﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenTK3DBase
{
    /// <summary>
    /// Represents the 6 cardinal 3D directions
    /// Right(X+), Left(X-)
    /// Up(Y+), Down(Y-)
    /// Forward(Z+), Back(Z-)
    /// </summary>
    public enum Direction
    {
        Right,//X+
        Left,//X-
        Up,//Y+
        Down,//Y-
        Forward,//Z+
        Back//Z-
    }
}
