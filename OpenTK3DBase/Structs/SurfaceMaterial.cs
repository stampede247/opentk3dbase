﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;
using System.Drawing;

namespace OpenTK3DBase
{
    public struct SurfaceMaterial
    {
        public static SurfaceMaterial Default
        {
            get
            {
                return new SurfaceMaterial()
                {
                    ambientColor = new Vector4(0.05f, 0.05f, 0.05f, 1.0f),
                    diffuseColor = new Vector4(1f, 1f, 1f, 1.0f),
                    specularColor = new Vector4(1.0f, 1.0f, 1.0f, 1.0f),
                    shininess = 5,
                };
            }
        }

        public Vector4 ambientColor;
        public Vector4 diffuseColor;
        public Vector4 specularColor;
        public float shininess;

        /// <summary>
        /// Sets the ambient and diffuse color to 0.2x and 0.8x the input color
        /// Gives the appearance of a colored object
        /// </summary>
        public Color OverallColor
        {
            set
            {
                this.ambientColor = Calc.FromColor(value) * 0.2f;
                this.diffuseColor = Calc.FromColor(value) * 0.8f;
            }
        }
    }
}
