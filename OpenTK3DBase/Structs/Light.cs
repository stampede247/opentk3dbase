﻿using OpenTK;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenTK3DBase
{
    public struct Light
    {
        public static Light Off
        {
            get
            {
                return new Light()
                {
                    isOn = false,
                    isInfinite = false,
                    position = new Vector3(0, 0, 0),
                    diffuseColor = new Vector4(1.0f, 1.0f, 1.0f, 1.0f),
                    specularColor = new Vector4(1.0f, 1.0f, 1.0f, 1.0f),
                    conAttenuation = 0.01f,
                    linAttenuation = 0.1f,
                    quadAttenuation = 0f,
                    spotCutoff = 180f,
                    spotExponent = 0,
                    spotDirection = Vector3.Normalize(new Vector3(1, 1, 1)),
                };
            }
        }
        public static Light Default
        {
            get
            {
                return new Light()
                {
                    isOn = true,
                    isInfinite = false,
                    position = new Vector3(0, 0, 0),
                    diffuseColor = new Vector4(1.0f, 1.0f, 1.0f, 1.0f),
                    specularColor = new Vector4(1.0f, 1.0f, 1.0f, 1.0f),
                    conAttenuation = 0.01f,
                    linAttenuation = 0.1f,
                    quadAttenuation = 0f,
                    spotCutoff = 180f,
                    spotExponent = 0,
                    spotDirection = Vector3.Normalize(new Vector3(1, 1, 1)),
                };
            }
        }
        public static Light DefaultSunlight
        {
            get
            {
                return new Light()
                {
                    isOn = true,
                    isInfinite = true,
                    position = Vector3.Normalize(new Vector3(0.3f, 2, 1)),
                    diffuseColor = new Vector4(1.0f, 1.0f, 1.0f, 1.0f),
                    specularColor = new Vector4(1.0f, 1.0f, 1.0f, 1.0f),
                    conAttenuation = 0f,
                    linAttenuation = 0f,
                    quadAttenuation = 0f,
                    spotCutoff = 180f,
                    spotExponent = 0,
                    spotDirection = Vector3.Normalize(new Vector3(1, 1, 1)),
                };
            }
        }

        public static Light OfColor(Color color)
        {
            return new Light()
            {
                isOn = true,
                isInfinite = false,
                position = new Vector3(0, 0, 0),
                diffuseColor = Calc.FromColor(color),
                specularColor = Calc.FromColor(color),
                conAttenuation = 0.01f,
                linAttenuation = 0.1f,
                quadAttenuation = 0f,
                spotCutoff = 180f,
                spotExponent = 0,
                spotDirection = Vector3.Normalize(new Vector3(1, 1, 1)),
            };
        }

        public bool isOn;
        /// <summary>
        /// If this is true then the position variable should be a normal vector for direction
        /// </summary>
        public bool isInfinite;
        /// <summary>
        /// if not infinite then this defines the position of the light in world space.
        /// if infinite this defines the direction of the infinite light. (should be a normalized vector)
        /// </summary>
        public Vector3 position;
        public Vector4 diffuseColor;
        public Vector4 specularColor;
        /// <summary>
        /// These three values are use to determine the distance that a point light reaches.
        /// Each one defines a sort of attenuation fall-off function. Higher values result is smaller lights.
        /// Values below 1 are good. A value of 0 in all 3 makes an infinitely reaching light.
        /// 0.01f, 0.1f, and 0.0f are defaults.
        /// </summary>
        public float conAttenuation, linAttenuation, quadAttenuation;
        /// <summary>
        /// This defines the angle reach of a spotlight. 180 is default and means no spotlight effect.
        /// </summary>
        public float spotCutoff;
        /// <summary>
        /// This defines the exponent of the function for attenuation in a spotlight. 0 is default
        /// </summary>
        public float spotExponent;
        /// <summary>
        /// This defines the pointing direction of the spotlight. Should be a normalized vector.
        /// Default is a normalized (1, 1, 1) vector.
        /// </summary>
        public Vector3 spotDirection;
    }
}
