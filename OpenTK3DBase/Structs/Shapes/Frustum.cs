﻿using OpenTK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenTK3DBase
{
    /// <summary>
    /// A class to hold values, properties, and aspects of a Frustum shape.
    /// </summary>
    public struct Frustum
    {
        /// <summary>
        /// The origin of the frustum
        /// </summary>
        public Vector3 origin;
        /// <summary>
        /// ANY point along the viewing Axis.
        /// </summary>
        public Vector3 target;
        /// <summary>
        /// A unit vector that defines the up direction of the Frustum
        /// NOTE: Not always perpendicular to TargetUnitVector. Check TrueUpVector for consistancy
        /// </summary>
        public Vector3 up;
        /// <summary>
        /// This is the field of view on the Y-axis
        /// </summary>
        public float fieldOfView;
        /// <summary>
        /// The distance from the origin to the near clipping plane
        /// </summary>
        public float near;
        /// <summary>
        /// The distance from the origin to the far clipping plane
        /// </summary>
        public float far;
        /// <summary>
        /// Ratio between width and height (width / height)
        /// </summary>
        public float aspectRatio;

        /// <summary>
        /// == fieldOfView * aspectRatio
        /// </summary>
        public float FieldOfViewY { get { return fieldOfView / aspectRatio; } }
        /// <summary>
        /// Same as fieldOfView
        /// </summary>
        public float FieldOfViewX { get { return fieldOfView; } }
        /// <summary>
        /// The width of the near clipping plane
        /// </summary>
        public float NearPlaneWidth
        {
            get
            {
                return (float)(2f * Math.Tan(fieldOfView / 2f) * near * aspectRatio);
            }
        }
        /// <summary>
        /// The height of the near clipping plane
        /// </summary>
        public float NearPlaneHeight
        {
            get
            {
                return (float)(2f * Math.Tan(fieldOfView / 2f) * near);
            }
        }
        /// <summary>
        /// The width of the far clipping plane
        /// </summary>
        public float FarPlaneWidth
        {
            get
            {
                return (float)(2f * Math.Tan(fieldOfView / 2f) * far * aspectRatio);
            }
        }
        /// <summary>
        /// The height of the far clipping plane
        /// </summary>
        public float FarPlaneHeight
        {
            get
            {
                return (float)(2f * Math.Tan(fieldOfView / 2f) * far);
            }
        }
        /// <summary>
        /// A unit vector that points towards the target point.
        /// Vector3.Normalize is used
        /// </summary>
        public Vector3 TargetUnitVector
        {
            get
            {
                return Vector3.Normalize(target - origin);
            }
        }
        /// <summary>
        /// "up" might not actually be perpendicular to the target position
        /// This returns a unit vector that is perpendicular to the TargetUnitVector
        /// </summary>
        public Vector3 TrueUpVector
        {
            get
            {
                return Vector3.Normalize(Vector3.Cross(Vector3.Cross(target - origin, up), target - origin));
            }
        }
        /// <summary>
        /// A normalized unit vector perpendicular to the up vector and TargetUnitVector.
        /// (target - origin) x up
        /// </summary>
        public Vector3 RightUnitVector
        {
            get
            {
                return Vector3.Normalize(Vector3.Cross(target - origin, up));
            }
        }
        /// <summary>
        /// The near clipping plane
        /// </summary>
        public Plane NearClippingPlane
        {
            get
            {
                return new Plane(TargetUnitVector, origin + TargetUnitVector * near);
            }
        }
        /// <summary>
        /// The far clipping plane
        /// </summary>
        public Plane FarClippingPlane
        {
            get
            {
                return new Plane(TargetUnitVector, origin + TargetUnitVector * far);
            }
        }

        /// <summary>
        /// The Top Left point on the near clipping plane
        /// </summary>
        public Vector3 NearTopLeft
        {
            get
            {
                return origin + TargetUnitVector * near +
                    -(NearPlaneWidth / 2f) * RightUnitVector +
                    -(NearPlaneHeight / 2f) * TrueUpVector;
            }
        }
        /// <summary>
        /// The Top Right point on the near clipping plane
        /// </summary>
        public Vector3 NearTopRight
        {
            get
            {
                return origin + TargetUnitVector * near +
                    (NearPlaneWidth / 2f) * RightUnitVector +
                    -(NearPlaneHeight / 2f) * TrueUpVector;
            }
        }
        /// <summary>
        /// The Bottom Left point on the near clipping plane
        /// </summary>
        public Vector3 NearBottomLeft
        {
            get
            {
                return origin + TargetUnitVector * near +
                    -(NearPlaneWidth / 2f) * RightUnitVector +
                    (NearPlaneHeight / 2f) * TrueUpVector;
            }
        }
        /// <summary>
        /// The Bottom Right point on the near clipping plane
        /// </summary>
        public Vector3 NearBottomRight
        {
            get
            {
                return origin + TargetUnitVector * near +
                    (NearPlaneWidth / 2f) * RightUnitVector +
                    (NearPlaneHeight / 2f) * TrueUpVector;
            }
        }
        /// <summary>
        /// The Top Left point on the far clipping plane
        /// </summary>
        public Vector3 FarTopLeft
        {
            get
            {
                return origin + TargetUnitVector * far +
                    -(FarPlaneWidth / 2f) * RightUnitVector +
                    -(FarPlaneHeight / 2f) * TrueUpVector;
            }
        }
        /// <summary>
        /// The Top Right point on the far clipping plane
        /// </summary>
        public Vector3 FarTopRight
        {
            get
            {
                return origin + TargetUnitVector * far +
                    (FarPlaneWidth / 2f) * RightUnitVector +
                    -(FarPlaneHeight / 2f) * TrueUpVector;
            }
        }
        /// <summary>
        /// The Bottom Left point on the far clipping plane
        /// </summary>
        public Vector3 FarBottomLeft
        {
            get
            {
                return origin + TargetUnitVector * far +
                    -(FarPlaneWidth / 2f) * RightUnitVector +
                    (FarPlaneHeight / 2f) * TrueUpVector;
            }
        }
        /// <summary>
        /// The Bottom Right point on the far clipping plane
        /// </summary>
        public Vector3 FarBottomRight
        {
            get
            {
                return origin + TargetUnitVector * far +
                    (FarPlaneWidth / 2f) * RightUnitVector +
                    (FarPlaneHeight / 2f) * TrueUpVector;
            }
        }

        /// <summary>
        /// Default Frustum initializer
        /// </summary>
        /// <param name="origin">The point from which the frustum expands</param>
        /// <param name="target">A point that the frustum points towards (distance doesn't matter)</param>
        /// <param name="up">A unit vector that defines the up direction for the frustum (90 deg angle between it and Target vector is prefered, check TrueUpVector for reliability)</param>
        /// <param name="fieldOfView">Angle between center and top/bottom side of the frustum (Y-axis feild of view)</param>
        /// <param name="near">distance to the near clipping plane</param>
        /// <param name="far">distance to the far clipping plane</param>
        /// <param name="aspectRatio">Ratio between width and height of the frustum. Width / Height</param>
        public Frustum(Vector3 origin, Vector3 target, Vector3 up, float fieldOfView, float near, float far, float aspectRatio)
        {
            this.origin = origin;
            this.target = target;
            this.up = up;
            this.fieldOfView = fieldOfView;
            this.near = near;
            this.far = far;
            this.aspectRatio = aspectRatio;
        }

        /// <summary>
        /// Uses Matrix.CreatePerspectiveFieldOfView to creaat a projection matrix that matches this frustum
        /// </summary>
        /// <returns>A Projection matrix</returns>
        public Matrix4 CreateProjectionMatrix()
        {
            return Matrix4.CreatePerspectiveFieldOfView(FieldOfViewY, aspectRatio, near, far);
        }
        /// <summary>
        /// Uses Matrix.LookAt to creaat a View matrix that matches this frustum
        /// </summary>
        /// <returns>A View matrix</returns>
        public Matrix4 CreateViewMatrix()
        {
            return Matrix4.LookAt(origin, target, up);
        }
    }
}
