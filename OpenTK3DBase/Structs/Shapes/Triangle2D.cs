﻿using OpenTK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenTK3DBase
{
    /// <summary>
    /// Represents a 2D Triangle made from v1, v2, and v3
    /// </summary>
    public struct Triangle2D
    {
        public Vector2 v1, v2, v3;

        public bool IsClockwise
        {
            get
            {
                return !Line2D.IsOnLeft(new Line2D(v1, v2), v3);
            }
        }
        public Vector2 Center
        {
            get
            {
                return (v1 + v2 + v3) / 3f;
            }
        }
        /// <summary>
        /// Needs to be tested
        /// </summary>
        public float Area
        {
            get
            {
                float a = Calc.Distance(v1, v2);
                float b = Calc.Distance(v2, v3);
                float c = Calc.Distance(v3, v1);
                float s = (a + b + c) / 2f;
                return (float)Math.Sqrt(s * (s - a) * (s - b) * (s - c));
            }
        }
        public Line2D L1
        {
            get
            {
                return new Line2D(v1, v2);
            }
        }
        public Line2D L2
        {
            get
            {
                return new Line2D(v2, v3);
            }
        }
        public Line2D L3
        {
            get
            {
                return new Line2D(v3, v1);
            }
        }
        public Vector2 GetVertex(int index)
        {
            if (index < 0 || index > 2)
                throw new ArgumentOutOfRangeException("index");

            switch (index)
            {
                case 1:
                    return v2;
                case 2:
                    return v3;
                default:
                    return v1;
            };
        }
        public void SetVertex(int index, Vector2 value)
        {
            if (index < 0 || index > 2)
                throw new ArgumentOutOfRangeException("index");

            switch (index)
            {
                case 1:
                    v2 = value;
                    break;
                case 2:
                    v3 = value;
                    break;
                default:
                    v1 = value;
                    break;
            };
        }
        public Line2D GetEdge(int index)
        {
            if (index < 0 || index > 2)
                throw new ArgumentOutOfRangeException("index");

            return new Line2D(GetVertex(index), GetVertex((index + 1) % 3));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="rearrange">
        /// Will rearrange them to clockwise if
        /// they were defined counter clockwise
        /// </param>
        public Triangle2D(Vector2 v1, Vector2 v2, Vector2 v3, bool rearrange = false)
        {
            this.v1 = v1;
            this.v2 = v2;
            this.v3 = v3;
            if (rearrange && !IsClockwise)
            {
                ReverseOrder();
            }
        }

        public void ReverseOrder()
        {
            Vector2 v1Old = v1;
            this.v1 = v3;
            this.v3 = v1Old;
        }

        public static Triangle2D RotateAround(Triangle2D triangle, Vector2 origin, float radians)
        {
            return new Triangle2D(
                Calc.RotateAround(triangle.v1, origin, radians),
                Calc.RotateAround(triangle.v2, origin, radians),
                Calc.RotateAround(triangle.v3, origin, radians));
        }
        public void RotateAround(Vector2 origin, float radians)
        {
            this.v1 = Calc.RotateAround(this.v1, origin, radians);
            this.v2 = Calc.RotateAround(this.v2, origin, radians);
            this.v3 = Calc.RotateAround(this.v3, origin, radians);
        }

        public static bool Contains(Triangle2D triangle, Vector2 point)
        {
            if (triangle.IsClockwise)
            {
                return (
                    !triangle.L1.IsOnLeft(point) &&
                    !triangle.L2.IsOnLeft(point) &&
                    !triangle.L3.IsOnLeft(point));
            }
            else
            {
                return (
                    triangle.L1.IsOnLeft(point) &&
                    triangle.L2.IsOnLeft(point) &&
                    triangle.L3.IsOnLeft(point));
            }
        }
        public bool Contains(Vector2 point)
        {
            if (IsClockwise)
            {
                return (
                    !L1.IsOnLeft(point) &&
                    !L2.IsOnLeft(point) &&
                    !L3.IsOnLeft(point));
            }
            else
            {
                return (
                    L1.IsOnLeft(point) &&
                    L2.IsOnLeft(point) &&
                    L3.IsOnLeft(point));
            }
        }

        public static Triangle2D operator +(Triangle2D triangle, Vector2 offset)
        {
            return new Triangle2D(triangle.v1 + offset, triangle.v2 + offset, triangle.v3 + offset);
        }
        public static Triangle2D operator -(Triangle2D triangle, Vector2 offset)
        {
            return new Triangle2D(triangle.v1 - offset, triangle.v2 - offset, triangle.v3 - offset);
        }
        public static explicit operator Polygon2D(Triangle2D t)
        {
            Polygon2D poly = new Polygon2D(3);
            poly.verts[0] = t.v1;
            poly.verts[1] = t.v2;
            poly.verts[2] = t.v3;

            return poly;
        }
    }
}
