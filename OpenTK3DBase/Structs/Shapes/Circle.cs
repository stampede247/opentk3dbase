﻿using OpenTK;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenTK3DBase
{
    /// <summary>
    /// Represents a 2D circle using the center and radius
    /// </summary>
    public struct Circle
    {
        public Vector2 center;
        public float radius;

        public float Area
        {
            get
            {
                return (float)(MathHelper.Pi * Math.Pow(radius, 2));
            }
        }
        public float Circumference
        {
            get
            {
                return (float)(2 * MathHelper.Pi * radius);
            }
        }
        /// <summary>
        /// A rectangle that encompasses the circle
        /// </summary>
        public RectangleF ColRec
        {
            get
            {
                return new RectangleF(center.X - radius, center.Y - radius, radius * 2, radius * 2);
            }
        }
        /// <summary>
        /// Returns the biggest rectangle that fits inside the circle
        /// Needs to be tested
        /// </summary>
        public RectangleF InnerRec
        {
            get
            {
                float offset = (float)(Math.Cos(MathHelper.PiOver2) * radius);
                return new RectangleF(center.X - offset, center.Y - offset, offset * 2, offset * 2);
            }
        }

        public Circle(Vector2 center, float radius)
        {
            this.center = center;
            this.radius = radius;
        }
        public Circle(RectangleF rec)
        {
            this.radius = Math.Max(
                Calc.Distance(new Vector2(rec.X, rec.Y), new Vector2(rec.Right, rec.Bottom)),
                Calc.Distance(new Vector2(rec.Right, rec.Y), new Vector2(rec.X, rec.Bottom)));

            this.center = new Vector2(rec.X + rec.Width / 2f, rec.Y + rec.Height / 2f);
        }
        public Circle(Rectangle rec)
        {
            this.radius = Math.Max(
                Calc.Distance(new Vector2(rec.X, rec.Y), new Vector2(rec.Right, rec.Bottom)),
                Calc.Distance(new Vector2(rec.Right, rec.Y), new Vector2(rec.X, rec.Bottom)));

            this.center = new Vector2(rec.X + rec.Width / 2f, rec.Y + rec.Height / 2f);
        }

        public static bool Intersects(Circle c1, Circle c2)
        {
            return (Calc.Distance(c1.center, c2.center) < c1.radius + c2.radius);
        }
        public bool Intersects(Circle circle)
        {
            return (Calc.Distance(this.center, circle.center) < this.radius + circle.radius);
        }

        public static bool IsInside(Circle circle, Vector2 point)
        {
            return (Calc.Distance(circle.center, point) < circle.radius);
        }
        public bool IsInside(Vector2 point)
        {
            return (Calc.Distance(this.center, point) < this.radius);
        }
        public bool IsInside(float x, float y)
        {
            return (Calc.Distance(this.center, new Vector2(x, y)) < this.radius);
        }

        public static Circle RotateAround(Circle circle, Vector2 origin, float radians)
        {
            return new Circle(Calc.RotateAround(circle.center, origin, radians), circle.radius);
        }
        public void RotateAround(Vector2 origin, float radians)
        {
            this.center = Calc.RotateAround(this.center, origin, radians);
        }
        public static Circle Translate(Circle circle, Vector2 offset)
        {
            return circle + offset;
        }
        public void Translate(Vector2 offset)
        {
            this.center += offset;
        }

        public static Circle FromRectangle(Rectangle rec)
        {
            float radius = Math.Max(
                Calc.Distance(new Vector2(rec.X, rec.Y), new Vector2(rec.Right, rec.Bottom)),
                Calc.Distance(new Vector2(rec.Right, rec.Y), new Vector2(rec.X, rec.Bottom)));

            return new Circle(new Vector2(rec.X + rec.Width / 2f, rec.Y + rec.Height / 2f), radius);
        }
        public static Circle FromRectangle(RectangleF rec)
        {
            float radius = Math.Max(
                Calc.Distance(new Vector2(rec.X, rec.Y), new Vector2(rec.Right, rec.Bottom)),
                Calc.Distance(new Vector2(rec.Right, rec.Y), new Vector2(rec.X, rec.Bottom)));

            return new Circle(new Vector2(rec.X + rec.Width / 2f, rec.Y + rec.Height / 2f), radius);
        }

        public static Circle operator +(Circle circle, Vector2 offset)
        {
            return new Circle(circle.center + offset, circle.radius);
        }
        public static Circle operator -(Circle circle, Vector2 offset)
        {
            return new Circle(circle.center - offset, circle.radius);
        }
    }
}
