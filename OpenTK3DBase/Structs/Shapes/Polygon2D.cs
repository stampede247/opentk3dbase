﻿using OpenTK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenTK3DBase
{
    /// <summary>
    /// Represents a 2D polygon with an arbitrary number of points
    /// </summary>
    public struct Polygon2D
    {
        public Vector2[] verts;

        public int NumVerts
        {
            get
            {
                return verts.Length;
            }
        }
        /// <summary>
        /// A complex polygon is one where lines intersect
        /// </summary>
        public bool IsComplex
        {
            get
            {
                for (int i1 = 0; i1 < NumVerts - 1; i1++)
                {
                    int i2 = (i1 + 1) % NumVerts;
                    for (int i3 = i1 + 1; i3 < NumVerts; i3++)
                    {
                        int i4 = (i3 + 1) % NumVerts;
                        Vector2 intersect;
                        if (Calc.LineVLine(verts[i1], verts[i2], verts[i3], verts[i4], out intersect, false))
                        {
                            return true;
                        }
                    }
                }

                return false;
            }
        }
        /// <summary>
        /// Might not work?
        /// </summary>
        public bool IsClockwise
        {
            get
            {
                int j, k;
                int count = 0;
                double z;

                if (NumVerts < 3)
                    return false;

                for (int i = 0; i < NumVerts; i++)
                {
                    j = (i + 1) % NumVerts;
                    k = (i + 2) % NumVerts;
                    z = (verts[j].X - verts[i].X) * (verts[k].Y - verts[j].Y);
                    z -= (verts[j].Y - verts[i].Y) * (verts[k].X - verts[j].X);

                    if (z < 0)
                        count--;
                    else if (z > 0)
                        count++;
                }

                return (count < 0);
            }
        }
        public bool IsConvex
        {
            get
            {
                int j, k;
                int flag = 0;
                double z;

                if (NumVerts < 3)
                    return true;

                for (int i = 0; i < NumVerts; i++)
                {
                    j = (i + 1) % NumVerts;
                    k = (i + 2) % NumVerts;
                    z = (verts[j].X - verts[i].X) * (verts[k].Y - verts[j].Y);
                    z -= (verts[j].Y - verts[i].Y) * (verts[k].X - verts[j].X);

                    if (z < 0)
                        flag |= 1;
                    else if (z > 0)
                        flag |= 2;

                    if (flag == 3)
                        return false;
                }

                if (flag != 0)
                    return true;
                else
                {
                    //Colinear points but we will return true
                    return true;
                }
            }
        }
        public Vector2 Center
        {
            get
            {
                Vector2 total = Vector2.Zero;
                for (int i = 0; i < NumVerts; i++)
                {
                    total += verts[i];
                }

                return total / (float)NumVerts;
            }
        }
        public Vector2 GetVertex(int index)
        {
            if (index < 0)
                return verts[0];
            if (index >= NumVerts)
                return verts[NumVerts - 1];

            return verts[index];
        }
        public Line2D GetEdge(int index)
        {
            if (index < 0)
                return new Line2D(verts[0], verts[1]);
            if (index >= NumVerts)
                return new Line2D(verts[NumVerts - 2], verts[NumVerts - 1]);

            return new Line2D(verts[index], verts[(index + 1) % NumVerts]);
        }

        public Polygon2D(int numberOfVertices)
        {
            verts = new Vector2[numberOfVertices];
        }
        public Polygon2D(Vector2 p1, Vector2? p2 = null, Vector2? p3 = null, Vector2? p4 = null, Vector2? p5 = null)
        {
            int num = 1;
            if (p2.HasValue)
                num++;
            if (p3.HasValue)
                num++;
            if (p4.HasValue)
                num++;
            if (p5.HasValue)
                num++;

            verts = new Vector2[num];

            verts[0] = p1;
            int i = 1;
            if (p2.HasValue)
            {
                verts[i] = p2.Value;
                i++;
            }
            if (p3.HasValue)
            {
                verts[i] = p3.Value;
                i++;
            }
            if (p4.HasValue)
            {
                verts[i] = p4.Value;
                i++;
            }
            if (p5.HasValue)
            {
                verts[i] = p5.Value;
                i++;
            }
        }
        public Polygon2D(Vector2[] vertices)
        {
            this.verts = vertices;
        }

    }
}
