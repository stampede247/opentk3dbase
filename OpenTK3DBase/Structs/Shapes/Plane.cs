﻿using OpenTK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenTK3DBase
{
    /// <summary>
    /// A class that represents a 3D infinite plane
    /// </summary>
    public struct Plane
    {
        /// <summary>
        /// A unit vector extending perpendicular to the surface of the plane
        /// </summary>
        public Vector3 normal;
        /// <summary>
        /// The distance from the origin. May be negative
        /// </summary>
        public float offset;

        public Plane(Vector3 normal, float offset)
        {
            this.normal = normal;
            if (normal.LengthSquared != 1)
                normal.Normalize();
            this.offset = offset;
        }
        public Plane(Vector3 normal, Vector3 position)
        {
            this.normal = normal;
            if (normal.LengthSquared != 1)
                normal.Normalize();
            this.offset = Vector3.Dot(position, normal);//TODO: TEST ME
        }
    }
}
