﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenTK3DBase
{
    /// <summary>
    /// Represents a 1 dimensional segment between a min and a max
    /// </summary>
    public struct Range
    {
        public float min, max;

        public float Mid
        {
            get
            {
                return (min + max) / 2f;
            }
        }
        public float Length
        {
            get
            {
                return max - min;
            }
        }

        /// <summary>
        /// val1 != min always because it automatically finds the smaller
        /// and greater values and sets them accordingly
        /// </summary>
        public Range(float val1, float val2)
        {
            min = Math.Min(val1, val2);
            max = Math.Max(val1, val2);
        }

        public static bool IsInside(Range range, float value, bool inclusive)
        {
            if (inclusive)
            {
                return (value >= range.min && value <= range.max);
            }
            else
            {
                return (value > range.min && value < range.max);
            }
        }
        public bool IsInside(float value, bool inclusive)
        {
            if (inclusive)
            {
                return (value >= min && value <= max);
            }
            else
            {
                return (value > min && value < max);
            }
        }
        public static bool Intersects(Range r1, Range r2, bool inclusive)
        {
            if (inclusive)
            {
                return (r1.max >= r2.min && r1.min <= r2.max);
            }
            else
            {
                return (r1.max > r2.min && r1.min < r2.max);
            }
        }
        public bool Intersects(Range range, bool inclusive)
        {
            if (inclusive)
            {
                return (this.max >= range.min && this.min <= range.max);
            }
            else
            {
                return (this.max > range.min && this.min < range.max);
            }
        }

        /// <summary>
        /// Essentially returns the amount these ranges overlap with each other.
        /// </summary>
        /// <param name="other"></param>
        /// <returns>a signed value of how to translate range1 to
        /// resolve the overlap</returns>
        public static float OverlapAmount(Range range1, Range range2)
        {
            if (range1.Mid >= range2.Mid)
            {
                return range2.max - range1.min;
            }
            else
            {
                return -(range1.max - range2.min);
            }
        }
        /// <summary>
        /// Essentially returns the amount this range overlaps with other
        /// </summary>
        /// <param name="other"></param>
        /// <returns>a signed value of how to translate this range to
        /// resolve the overlap</returns>
        public float OverlapAmount(Range other)
        {
            if (this.Mid >= other.Mid)
            {
                return other.max - this.min;
            }
            else
            {
                return -(this.max - other.min);
            }
        }
        /// <summary>
        /// Returns a range that represents the area in which 2 ranges overlap.
        /// </summary>
        /// <returns>A range for overlap found. or new Range(0, 0) if no intersection</returns>
        public static Range Overlap(Range range1, Range range2)//TODO: Add to HeightmapPOC
        {
            if (range1.min <= range2.max && range2.min <= range1.max)
            {
                return new Range(Math.Max(range1.min, range2.min), Math.Min(range1.max, range2.max));
            }
            else
            {
                return new Range(0, 0);
            }
        }

        /// <summary>
        /// Use this for non-intersecting ranges
        /// </summary>
        /// <param name="?"></param>
        /// <returns>Translation needed to intersect with other</returns>
        public float TranslationTo(Range other)
        {
            if (this.max <= other.min)
                return other.min - this.max;
            else
                return this.min - other.max;
        }

        public static Range operator +(Range r, float val)
        {
            return new Range(r.min + val, r.max + val);
        }
        public static Range operator -(Range r, float val)
        {
            return new Range(r.min - val, r.max - val);
        }
    }
}
