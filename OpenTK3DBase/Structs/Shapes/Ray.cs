﻿using OpenTK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenTK3DBase
{
    /// <summary>
    /// Represents an infinite line starting at origin and heading in a specific direction
    /// </summary>
    public struct Ray
    {
        /// <summary>
        /// The origin of the ray
        /// </summary>
        public Vector3 origin;
        /// <summary>
        /// A unit vector representing the direction from origin
        /// </summary>
        public Vector3 direction;

        /// <param name="target">Does not have to be a unit vector. direction = (target - origin) normalized</param>
        public Ray(Vector3 origin, Vector3 target)
        {
            this.origin = origin;
            this.direction = target - origin;
            if (direction.LengthSquared != 1)
                direction.Normalize();
        }
    }
}
