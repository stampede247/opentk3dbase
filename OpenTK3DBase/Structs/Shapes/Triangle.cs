﻿using OpenTK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenTK3DBase
{
    /// <summary>
    /// Represents a 3D triangle with points v1, v2, and v3
    /// </summary>
    public struct Triangle
    {
        public Vector3 v1, v2, v3;

        /// <summary>
        /// Returns a unit vector representing the clockwise front face
        /// (v3-v1)x(v2-v1) normalized
        /// </summary>
        public Vector3 Normal
        {
            get
            {
                return Vector3.Normalize(Vector3.Cross(v3 - v1, v2 - v1));
            }
        }
        /// <summary>
        /// side between v1 and v2
        /// </summary>
        public LineSegment Side1 { get { return new LineSegment(v1, v2); } }
        /// <summary>
        /// side between v2 and v3
        /// </summary>
        public LineSegment Side2 { get { return new LineSegment(v2, v3); } }
        /// <summary>
        /// side between v3 and v1
        /// </summary>
        public LineSegment Side3 { get { return new LineSegment(v3, v1); } }

        public Triangle(Vector3 v1, Vector3 v2, Vector3 v3)
        {
            this.v1 = v1;
            this.v2 = v2;
            this.v3 = v3;
        }

        /// <summary>
        /// Returns one of the 3 vertices.
        /// NOTE: 0 based index
        /// 
        /// Throws: ArgumentOutOfRangeException if < 0 or > 2
        /// </summary>
        /// <param name="index">0, 1, or 2 (NOT 1, 2, and 3!!)</param>
        public Vector3 GetVertex(int index)
        {
            switch (index)
            {
                case 0:
                    return v1;
                case 1:
                    return v2;
                case 2:
                    return v3;

                default:
                    throw new ArgumentOutOfRangeException("index");
            };
        }
        /// <summary>
        /// Sets one of the 3 vertices.
        /// NOTE: 0 based index
        /// 
        /// Throws: ArgumentOutOfRangeException if < 0 or > 2
        /// </summary>
        /// <param name="index">0, 1, or 2 (NOT 1, 2, and 3!!)</param>
        /// <param name="value">The new value of the vertex</param>
        public void SetVertex(int index, Vector3 value)
        {
            switch (index)
            {
                case 0:
                    v1 = value;
                    break;
                case 1:
                    v2 = value;
                    break;
                case 2:
                    v3 = value;
                    break;

                default:
                    throw new ArgumentOutOfRangeException("index");
            };
        }
        /// <summary>
        /// Returns one of the 3 sides.
        /// NOTE: 0 based index
        /// 
        /// Throws: ArgumentOutOfRangeException if < 0 or > 2
        /// </summary>
        /// <param name="index">0, 1, or 2 (NOT 1, 2, and 3!!)</param>
        public LineSegment GetSide(int index)
        {
            switch (index)
            {
                case 0:
                    return Side1;
                case 1:
                    return Side2;
                case 2:
                    return Side3;

                default:
                    throw new ArgumentOutOfRangeException("index");
            };
        }
    }
}
