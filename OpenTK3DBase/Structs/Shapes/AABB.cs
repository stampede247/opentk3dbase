﻿using OpenTK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenTK3DBase
{
    public struct AABB
    {
        /// <summary>
        /// Represents the 3 minimun values (e.g. top left front corner)
        /// </summary>
        public Vector3 min;
        /// <summary>
        /// Represents the 3 maximum values (e.g. bottom right back corner)
        /// </summary>
        public Vector3 max;

        public Vector3 Size
        {
            get
            {
                return max - min;
            }
        }
        public Vector3 Center
        {
            get
            {
                return (max + min) / 2f;
            }
        }
        /// <summary>
        /// X-axis size
        /// max.X - min.X
        /// </summary>
        public float Width { get { return max.X - min.X; } }
        /// <summary>
        /// Y-axis size
        /// max.Y - min.Y
        /// </summary>
        public float Height { get { return max.Y - min.Y; } }
        /// <summary>
        /// Z-axis size
        /// max.Z - min.Z
        /// </summary>
        public float Depth { get { return max.Z - min.Z; } }
        /// <summary>
        /// Volume of the Bounding Box
        /// Width * Height * Depth
        /// </summary>
        public float Volume { get { return Width * Height * Depth; } }
        /// <summary>
        /// Returns the total surface area of all sides of the AABB
        /// </summary>
        public float SurfaceArea { get { return Width * Height * 2 + Width * Depth * 2 + Height * Depth * 2; } }
        /// <summary>
        /// One of the 8 vertices of the Bounding Box
        /// (min.X, min.Y, min.Z)
        /// Same as min
        /// </summary>
        public Vector3 BottomLeftFront { get { return new Vector3(min.X, min.Y, min.Z); } }
        /// <summary>
        /// One of the 8 vertices of the Bounding Box
        /// (max.X, min.Y, min.Z)
        /// </summary>
        public Vector3 BottomRightFront { get { return new Vector3(max.X, min.Y, min.Z); } }
        /// <summary>
        /// One of the 8 vertices of the Bounding Box
        /// (min.X, min.Y, max.Z)
        /// </summary>
        public Vector3 BottomLeftBack { get { return new Vector3(min.X, min.Y, max.Z); } }
        /// <summary>
        /// One of the 8 vertices of the Bounding Box
        /// (max.X, min.Y, max.Z)
        /// </summary>
        public Vector3 BottomRightBack { get { return new Vector3(max.X, min.Y, max.Z); } }
        /// <summary>
        /// One of the 8 vertices of the Bounding Box
        /// (min.X, max.Y, min.Z)
        /// </summary>
        public Vector3 TopLeftFront { get { return new Vector3(min.X, max.Y, min.Z); } }
        /// <summary>
        /// One of the 8 vertices of the Bounding Box
        /// (max.X, max.Y, min.Z)
        /// </summary>
        public Vector3 TopRightFront { get { return new Vector3(max.X, max.Y, min.Z); } }
        /// <summary>
        /// One of the 8 vertices of the Bounding Box
        /// (min.X, max.Y, max.Z)
        /// </summary>
        public Vector3 TopLeftBack { get { return new Vector3(min.X, max.Y, max.Z); } }
        /// <summary>
        /// One of the 8 vertices of the Bounding Box
        /// (max.X, max.Y, max.Z)
        /// same as max
        /// </summary>
        public Vector3 TopRightBack { get { return new Vector3(max.X, max.Y, max.Z); } }

        /// <summary>
        /// Creates an Axis-Aligned Bounding Box with min at (x,y,z) and max at (x + width, y + height, z + depth)
        /// </summary>
        /// <param name="x">min-X</param>
        /// <param name="y">min-Y</param>
        /// <param name="z">min-Z</param>
        /// <param name="width">X-axis size</param>
        /// <param name="height">Y-axis size</param>
        /// <param name="depth">Z-axis size</param>
        public AABB(float x, float y, float z, float width, float height, float depth)
        {
            this.min = new Vector3(x, y, z);
            this.max = new Vector3(x + width, y + height, z + depth);
        }
        /// <summary>
        /// Creates an Axis-Aligned Bounding Box that contains two points
        /// </summary>
        /// <param name="p1">A point inside/on the edge of the AABB</param>
        /// <param name="p2">Another point inside/on the edge of the AABB</param>
        public AABB(Vector3 p1, Vector3 p2)
        {
            this.min = new Vector3(
                Math.Min(p1.X, p2.X),
                Math.Min(p1.Y, p2.Y),
                Math.Min(p1.Z, p2.Z));
            this.max = new Vector3(
                Math.Max(p1.X, p2.X),
                Math.Max(p1.Y, p2.Y),
                Math.Max(p1.Z, p2.Z));
        }
    }
}
