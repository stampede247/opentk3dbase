﻿using OpenTK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenTK3DBase
{
    /// <summary>
    /// Represents a four sided 3D object.
    /// The points should represent a flat face
    /// </summary>
    public struct Quad
    {
        /// <summary>
        /// In order to be rendered correctly please ensure that these points all lie on the same plane
        /// </summary>
        public Vector3 v1, v2, v3, v4;

        /// <summary>
        /// Returns a unit vector representing the clockwise front face
        /// (v4-v1)x(v2-v1) normalized
        /// </summary>
        public Vector3 Normal
        {
            get
            {
                return Vector3.Normalize(Vector3.Cross(v4 - v1, v2 - v1));
            }
        }
        /// <summary>
        /// side between v1 and v2
        /// </summary>
        public LineSegment Side1 { get { return new LineSegment(v1, v2); } }
        /// <summary>
        /// side between v2 and v3
        /// </summary>
        public LineSegment Side2 { get { return new LineSegment(v2, v3); } }
        /// <summary>
        /// side between v3 and v4
        /// </summary>
        public LineSegment Side3 { get { return new LineSegment(v3, v4); } }
        /// <summary>
        /// side between v4 and v1
        /// </summary>
        public LineSegment Side4 { get { return new LineSegment(v4, v1); } }

        public Quad(Vector3 v1, Vector3 v2, Vector3 v3, Vector3 v4)
        {
            this.v1 = v1;
            this.v2 = v2;
            this.v3 = v3;
            this.v4 = v4;
        }

        /// <summary>
        /// Returns one of the 4 vertices.
        /// NOTE: 0 based index
        /// 
        /// Throws: ArgumentOutOfRangeException if < 0 or > 3
        /// </summary>
        /// <param name="index">0, 1, 2 or 3 (NOT 1, 2, 3, and 4!!)</param>
        public Vector3 GetVertex(int index)
        {
            switch (index)
            {
                case 0:
                    return v1;
                case 1:
                    return v2;
                case 2:
                    return v3;
                case 3:
                    return v4;

                default:
                    throw new ArgumentOutOfRangeException("index");
            };
        }
        /// <summary>
        /// Sets one of the 4 vertices.
        /// NOTE: 0 based index
        /// 
        /// Throws: ArgumentOutOfRangeException if < 0 or > 3
        /// </summary>
        /// <param name="index">0, 1, 2 or 3 (NOT 1, 2, 3, and 4!!)</param>
        /// <param name="value">The new value of the vertex</param>
        public void SetVertex(int index, Vector3 value)
        {
            switch (index)
            {
                case 0:
                    v1 = value;
                    break;
                case 1:
                    v2 = value;
                    break;
                case 2:
                    v3 = value;
                    break;
                case 3:
                    v4 = value;
                    break;

                default:
                    throw new ArgumentOutOfRangeException("index");
            };
        }
        /// <summary>
        /// Returns one of the 4 sides.
        /// NOTE: 0 based index
        /// 0 = v1-v2
        /// 1 = v2-v3
        /// 2 = v2-v4
        /// 3 = v4-v1
        /// 
        /// Throws: ArgumentOutOfRangeException if < 0 or > 3
        /// </summary>
        /// <param name="index">0, 1, 2 or 3 (NOT 1, 2, 3, and 4!!)</param>
        public LineSegment GetSide(int index)
        {
            switch (index)
            {
                case 0:
                    return Side1;
                case 1:
                    return Side2;
                case 2:
                    return Side3;
                case 3:
                    return Side4;

                default:
                    throw new ArgumentOutOfRangeException("index");
            };
        }
    }
}
