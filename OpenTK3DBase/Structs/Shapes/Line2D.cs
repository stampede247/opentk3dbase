﻿using OpenTK;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenTK3DBase
{
    /// <summary>
    /// Represents a 2D line segment from p1 to p2
    /// </summary>
    public struct Line2D
    {
        public Vector2 p1, p2;

        public float Length
        {
            get
            {
                return Calc.Distance(p1, p2);
            }
        }
        public double HeadingDir
        {
            get
            {
                return Calc.DirectionTo(p1, p2);
            }
        }
        public float Slope
        {
            get
            {
                if (UndefinedSlope)
                {
                    Console.WriteLine("Tried to get slope of a line with undefined slope");
                    return float.MaxValue;
                }
                else
                {
                    return (p2.Y - p1.Y) / (p2.X - p1.X);
                }
            }
        }
        public Vector2 MidPoint
        {
            get
            {
                return (p1 + p2) / 2f;
            }
        }
        public Range XRange
        {
            get
            {
                return new Range(p1.X, p2.X);
            }
        }
        public Range YRange
        {
            get
            {
                return new Range(p1.Y, p2.Y);
            }
        }
        public bool UndefinedSlope
        {
            get { return p1.X == p2.X; }
        }
        public double LeftNormalDir
        {
            get
            {
                double val = HeadingDir - MathHelper.PiOver2;
                return (val < 0 ? val + MathHelper.Pi * 2 : val);
            }
        }
        public double RightNormalDir
        {
            get
            {
                double val = HeadingDir + MathHelper.PiOver2;
                return (val > MathHelper.Pi * 2 ? val - MathHelper.Pi * 2 : val);
            }
        }
        public Vector2 LeftNormal
        {
            get
            {
                return Calc.CirclePoint((float)(HeadingDir - MathHelper.PiOver2), 1f);
            }
        }
        public Vector2 RightNormal
        {
            get
            {
                return Calc.CirclePoint((float)(HeadingDir + MathHelper.PiOver2), 1f);
            }
        }
        public RectangleF BoundingRec
        {
            get
            {
                return new RectangleF(Math.Min(p1.X, p2.X), Math.Min(p1.Y, p2.Y),
                    Math.Max(p1.X, p2.X) - Math.Min(p1.X, p2.X), Math.Max(p1.Y, p2.Y) - Math.Min(p1.Y, p2.Y));
            }
        }
        public Vector2 GetPoint(int index)
        {
            if (index < 0 || index > 1)
                throw new ArgumentOutOfRangeException("index");

            return (index == 0) ? p1 : p2;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="rearrange">Will switch p1 and p2 if p1.X > p2.X when this is true</param>
        public Line2D(Vector2 p1, Vector2 p2, bool rearrange = false)
        {
            if (rearrange)
            {
                this.p1 = (p1.X <= p2.X) ? p1 : p2;
                this.p2 = (p1.X <= p2.X) ? p2 : p1;
            }
            else
            {
                this.p1 = p1;
                this.p2 = p2;
            }
        }

        public static bool Intersects(Line2D line1, Line2D line2, out Vector2 intersect)
        {
            return Calc.LineVLine(line1.p1, line1.p2, line2.p1, line2.p2, out intersect);
        }
        public bool Intersects(Line2D line, out Vector2 intersect)
        {
            return Calc.LineVLine(this.p1, this.p2, line.p1, line.p2, out intersect);
        }

        /// <summary>
        /// Returns a point between p1 and p2 when given a value
        /// between 0 and 1. 0 being p1 and 1 being p2 and 0.5
        /// being midpoint
        /// </summary>
        /// <param name="lerp">between 0 and 1</param>
        public static Vector2 GetLerpPoint(Line2D line, float lerp)
        {
            return line.p1 + (line.p2 - line.p1) * lerp;
        }
        /// <summary>
        /// Returns a point between p1 and p2 when given a value
        /// between 0 and 1. 0 being p1 and 1 being p2 and 0.5
        /// being midpoint
        /// </summary>
        /// <param name="lerp">between 0 and 1</param>
        public Vector2 GetLerpPoint(float lerp)
        {
            return p1 + (p2 - p1) * lerp;
        }
        /// <summary>
        /// Will return a value off the line if given
        /// an x that doesn't fall on the line
        /// </summary>
        public Vector2 GetPointAtX(float x, bool relative = false)
        {
            if (UndefinedSlope)
            {
                if (x < p1.X)
                    return p1;
                else
                    return p2;
            }
            float offset = relative ? (x) : (x - p1.X);
            return new Vector2(x, p1.Y + Slope * offset);
        }

        /// <summary>
        /// Returns whether the point is above 
        /// the (infinitely extending) line
        /// </summary>
        public static bool IsAbove(Line2D line, Vector2 point)
        {
            if (line.p2.X == line.p1.X)
            {
                if (line.p2.Y >= line.p1.Y)
                    return point.X <= line.p1.X;
                else
                    return point.X >= line.p1.X;
            }

            return (line.GetPointAtX(point.X).Y >= point.Y);
        }
        /// <summary>
        /// Returns whether the point is above 
        /// the (infinitely extending) line
        /// </summary>
        public bool IsAbove(Vector2 point)
        {
            if (p2.X == p1.X)
            {
                if (p2.Y >= p1.Y)
                    return point.X <= p1.X;
                else
                    return point.X >= p1.X;
            }

            return (GetPointAtX(point.X).Y >= point.Y);
            //float displace = point.X - p1.X;
            //return (displace * Slope + p1.Y <= point.Y);
        }
        /// <summary>
        /// Kind of line IsAbove but checks to see if the 
        /// point is on it's left going from p1 to p2
        /// </summary>
        public static bool IsOnLeft(Line2D line, Vector2 point)
        {
            if (line.p2.X == line.p1.X)
            {
                if (line.p2.Y >= line.p1.Y)
                    return point.X >= line.p1.X;
                else
                    return point.X <= line.p1.X;
            }

            if (Calc.WithinBounds(0, MathHelper.Pi, (float)(line.LeftNormalDir)))
                return !line.IsAbove(point);
            else
                return line.IsAbove(point);
        }
        /// <summary>
        /// Kind of line IsAbove but checks to see if the 
        /// point is on it's left going from p1 to p2
        /// </summary>
        public bool IsOnLeft(Vector2 point)
        {
            if (p2.X == p1.X)
            {
                if (p2.Y >= p1.Y)
                    return point.X >= p1.X;
                else
                    return point.X <= p1.X;
            }

            if (Calc.WithinBounds(0, MathHelper.Pi, (float)(LeftNormalDir)))
                return !IsAbove(point);
            else
                return IsAbove(point);
        }

        /// <summary>
        /// Finds a point on the line that (with testPoint) forms a perpendicular line
        /// This is the closest point on this line to testPoint
        /// </summary>
        public static Vector2 PerpPoint(Line2D line, Vector2 testPoint)
        {
            Vector2 p1 = line.p1;
            Vector2 p2 = line.p2;
            Vector2 p3 = testPoint;
            Vector2 p4 = testPoint + line.RightNormal;
            Vector2 intersection = Vector2.Zero;
            return new Vector2(
                ((p1.X * p2.Y - p1.Y * p2.X) * (p3.X - p4.X) - (p1.X - p2.X) * (p3.X * p4.Y - p3.Y * p4.X))
                / //---------------------------------------------------------------------------------------
                ((p1.X - p2.X) * (p3.Y - p4.Y) - (p1.Y - p2.Y) * (p3.X - p4.X)),

                ((p1.X * p2.Y - p1.Y * p2.X) * (p3.Y - p4.Y) - (p1.Y - p2.Y) * (p3.X * p4.Y - p3.Y * p4.X))
                / //---------------------------------------------------------------------------------------
                ((p1.X - p2.X) * (p3.Y - p4.Y) - (p1.Y - p2.Y) * (p3.X - p4.X)));
        }
        /// <summary>
        /// Finds a point on the line that (with testPoint) forms a perpendicular line
        /// This is the closest point on this line to testPoint
        /// </summary>
        public Vector2 PerpPoint(Vector2 testPoint)
        {
            Vector2 p1 = this.p1;
            Vector2 p2 = this.p2;
            Vector2 p3 = testPoint;
            Vector2 p4 = testPoint + this.RightNormal;
            Vector2 intersection = Vector2.Zero;
            return new Vector2(
                ((p1.X * p2.Y - p1.Y * p2.X) * (p3.X - p4.X) - (p1.X - p2.X) * (p3.X * p4.Y - p3.Y * p4.X))
                / //---------------------------------------------------------------------------------------
                ((p1.X - p2.X) * (p3.Y - p4.Y) - (p1.Y - p2.Y) * (p3.X - p4.X)),

                ((p1.X * p2.Y - p1.Y * p2.X) * (p3.Y - p4.Y) - (p1.Y - p2.Y) * (p3.X * p4.Y - p3.Y * p4.X))
                / //---------------------------------------------------------------------------------------
                ((p1.X - p2.X) * (p3.Y - p4.Y) - (p1.Y - p2.Y) * (p3.X - p4.X)));
        }

        public static Line2D RotateAround(Line2D line, Vector2 origin, float radians)
        {
            return new Line2D(Calc.RotateAround(line.p1, origin, radians), Calc.RotateAround(line.p2, origin, radians));
        }
        public void RotateAround(Vector2 origin, float radians)
        {
            this.p1 = Calc.RotateAround(this.p1, origin, radians);
            this.p2 = Calc.RotateAround(this.p2, origin, radians);
        }
        public static Line2D Translate(Line2D line, Vector2 offset)
        {
            return line + offset;
        }
        public void Translate(Vector2 offset)
        {
            this.p1 += offset;
            this.p2 += offset;
        }

        public static Line2D operator +(Line2D line, Vector2 offset)
        {
            return new Line2D(line.p1 + offset, line.p2 + offset);
        }
        public static Line2D operator -(Line2D line, Vector2 offset)
        {
            return new Line2D(line.p1 - offset, line.p2 - offset);
        }
    }
}
