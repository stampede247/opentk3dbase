﻿using OpenTK;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenTK3DBase
{
    /// <summary>
    /// Represents a 4 sides shape withs points v1, v2, v3, and v4
    /// </summary>
    public struct Quad2D
    {
        public Vector2 v1, v2, v3, v4;

        public bool IsComplex
        {
            get
            {
                for (int i1 = 0; i1 < 4 - 1; i1++)
                {
                    int i2 = (i1 + 1) % 4;
                    for (int i3 = i1 + 1; i3 < 4; i3++)
                    {
                        int i4 = (i3 + 1) % 4;
                        Vector2 intersect;
                        if (Calc.LineVLine(GetVertex(i1), GetVertex(i2), GetVertex(i3), GetVertex(i4), out intersect, false))
                        {
                            return true;
                        }
                    }
                }

                return false;
            }
        }
        public bool IsClockwise
        {
            get
            {
                int i, j, k;
                int count = 0;

                for (i = 0; i < 4; i++)
                {
                    j = (i + 1) % 4;
                    k = (i == 0) ? (3) : (i - 1);
                    bool isRightTurn = new Line2D(Vector2.Zero, GetVertex(j) - GetVertex(i)).IsOnLeft(GetVertex(i) - GetVertex(k));

                    count += (isRightTurn ? 1 : -1);
                }

                return (count >= 0);
            }
        }
        public bool IsConvex
        {
            get
            {
                int j, k;
                int flag = 0;
                double z;

                for (int i = 0; i < 4; i++)
                {
                    j = (i + 1) % 4;
                    k = (i + 2) % 4;
                    z = (GetVertex(j).X - GetVertex(i).X) * (GetVertex(k).Y - GetVertex(j).Y);
                    z -= (GetVertex(j).Y - GetVertex(i).Y) * (GetVertex(k).X - GetVertex(j).X);

                    if (z < 0)
                        flag |= 1;
                    else if (z > 0)
                        flag |= 2;

                    if (flag == 3)
                        return false;
                }

                if (flag != 0)
                    return true;
                else
                {
                    //Colinear points but we will return true
                    return true;
                }
            }
        }
        public Vector2 Center
        {
            get
            {
                return (v1 + v2 + v3 + v4) / 4f;
            }
        }
        public Vector2 GetVertex(int index)
        {
            if (index < 0 || index > 3)
                throw new ArgumentOutOfRangeException("index");

            switch (index)
            {
                case 1:
                    return v2;
                case 2:
                    return v3;
                case 3:
                    return v4;
                default:
                    return v1;
            };
        }
        public void SetVertex(int index, Vector2 value)
        {
            if (index < 0 || index > 3)
                throw new ArgumentOutOfRangeException("index");

            switch (index)
            {
                case 1:
                    v2 = value;
                    break;
                case 2:
                    v3 = value;
                    break;
                case 3:
                    v4 = value;
                    break;
                default:
                    v1 = value;
                    break;
            };
        }
        public Line2D GetEdge(int index)
        {
            if (index < 0 || index > 3)
                throw new ArgumentOutOfRangeException("index");

            return new Line2D(GetVertex(index), GetVertex((index + 1) % 4));
        }

        public Quad2D(Vector2 p1, Vector2 p2, Vector2 p3, Vector2 p4)
        {
            this.v1 = p1;
            this.v2 = p2;
            this.v3 = p3;
            this.v4 = p4;
        }
        public Quad2D(OBB2D rec)
        {
            this.v1 = rec.TopLeft;
            this.v2 = rec.TopRight;
            this.v3 = rec.BottomRight;
            this.v4 = rec.BottomLeft;
        }
        public Quad2D(Rectangle rec)
        {
            this.v1 = new Vector2(rec.X, rec.Y);
            this.v2 = new Vector2(rec.Right, rec.Y);
            this.v3 = new Vector2(rec.Right, rec.Bottom);
            this.v4 = new Vector2(rec.X, rec.Bottom);
        }
        public Quad2D(RectangleF rec)
        {
            this.v1 = new Vector2(rec.X, rec.Y);
            this.v2 = new Vector2(rec.Right, rec.Y);
            this.v3 = new Vector2(rec.Right, rec.Bottom);
            this.v4 = new Vector2(rec.X, rec.Bottom);
        }
        public Quad2D(Vector2[] verts)
        {
            if (verts.Length < 4 || verts.Length > 4)
                throw new ArgumentException("The vertex array was not 4 nodes long.");

            this.v1 = verts[0];
            this.v2 = verts[1];
            this.v3 = verts[2];
            this.v4 = verts[3];
        }

        public static explicit operator Polygon2D(Quad2D q)
        {
            Polygon2D poly = new Polygon2D(4);
            poly.verts[0] = q.v1;
            poly.verts[1] = q.v2;
            poly.verts[2] = q.v3;
            poly.verts[3] = q.v4;

            return poly;
        }
    }
}
