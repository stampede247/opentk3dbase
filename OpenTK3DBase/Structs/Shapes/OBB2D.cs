﻿using OpenTK;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenTK3DBase
{
    /// <summary>
    /// Represents an Oriented Bounding Box or rotated rectangle in 2 dimensions
    /// </summary>
    public struct OBB2D
    {
        public Vector2 center;
        public Vector2 size;
        public double rotation;

        public float X
        {
            get
            {
                return center.X;
            }
            set
            {
                center.X = value;
            }
        }
        public float Y
        {
            get
            {
                return center.Y;
            }
            set
            {
                center.Y = value;
            }
        }
        public float Width
        {
            get
            {
                return size.X;
            }
            set
            {
                size.X = value;
            }
        }
        public float Height
        {
            get
            {
                return size.Y;
            }
            set
            {
                size.Y = value;
            }
        }
        public float rotationF
        {
            get
            {
                return (float)rotation;
            }
            set
            {
                rotation = (double)value;
            }
        }
        public Vector2 HalfSize
        {
            get
            {
                return size / 2f;
            }
            set
            {
                size = value * 2f;
            }
        }
        public Vector2 TopLeft
        {
            get
            {
                float offset = (float)Calc.DirectionTo(Vector2.Zero, HalfSize) - MathHelper.PiOver4;
                return center + Calc.CirclePoint((float)rotation - MathHelper.PiOver4 * 3 + offset, HalfSize.Length);
            }
        }
        public Vector2 TopRight
        {
            get
            {
                float offset = (float)Calc.DirectionTo(Vector2.Zero, HalfSize) - MathHelper.PiOver4;
                return center + Calc.CirclePoint((float)rotation - MathHelper.PiOver4 - offset, HalfSize.Length);
            }
        }
        public Vector2 BottomLeft
        {
            get
            {
                float offset = (float)Calc.DirectionTo(Vector2.Zero, HalfSize) - MathHelper.PiOver4;
                return center + Calc.CirclePoint((float)rotation + MathHelper.PiOver4 * 3 - offset, HalfSize.Length);
            }
        }
        public Vector2 BottomRight
        {
            get
            {
                float offset = (float)Calc.DirectionTo(Vector2.Zero, HalfSize) - MathHelper.PiOver4;
                return center + Calc.CirclePoint((float)rotation + MathHelper.PiOver4 + offset, HalfSize.Length);
            }
        }
        public float MinX
        {
            get
            {
                return (float)Math.Min(TopLeft.X,
                              Math.Min(TopRight.X,
                              Math.Min(BottomLeft.X, BottomRight.X)));
            }
        }
        public float MinY
        {
            get
            {
                return (float)Math.Min(TopLeft.Y,
                              Math.Min(TopRight.Y,
                              Math.Min(BottomLeft.Y, BottomRight.Y)));
            }
        }
        public float MaxX
        {
            get
            {
                return (float)Math.Max(TopLeft.X,
                              Math.Max(TopRight.X,
                              Math.Max(BottomLeft.X, BottomRight.X)));
            }
        }
        public float MaxY
        {
            get
            {
                return (float)Math.Max(TopLeft.Y,
                              Math.Max(TopRight.Y,
                              Math.Max(BottomLeft.Y, BottomRight.Y)));
            }
        }
        public RectangleF FloatRec
        {
            get
            {
                if (rotation == 0.0)
                {
                    return new RectangleF(center.X - HalfSize.X, center.Y - HalfSize.Y, size.X, size.Y);
                }
                return new RectangleF(
                    (MinX), (MinY),
                    (MaxX - MinX), (MaxY - MinY));
            }
        }
        /// <summary>
        /// Used for seperating axis thereom
        /// </summary>
        public Vector2 AxisX
        {
            get
            {
                Vector2 axis = TopRight - TopLeft;
                return (axis / axis.LengthSquared);
            }
        }
        /// <summary>
        /// Used for seperating axis thereom
        /// </summary>
        public Vector2 AxisY
        {
            get
            {
                Vector2 axis = BottomLeft - TopLeft;
                return (axis / axis.LengthSquared);
            }
        }

        public OBB2D(Vector2 center, Vector2 size, float rotation = 0f)
        {
            this.center = center;
            this.rotation = rotation;
            this.size = size;
        }
        public OBB2D(Vector2 center, Vector2 size, double rotation = 0.0)
        {
            this.center = center;
            this.rotation = rotation;
            this.size = size;
        }
        public OBB2D(Vector2 center, float width, float height, float rotation = 0f)
        {
            this.center = center;
            this.rotation = rotation;
            this.size = new Vector2(width, height);
        }
        public OBB2D(Vector2 center, float width, float height, double rotation = 0.0)
        {
            this.center = center;
            this.rotation = rotation;
            this.size = new Vector2(width, height);
        }
        public OBB2D(float centerX, float centerY, float width, float height, float rotation = 0f)
        {
            this.center = new Vector2(centerX, centerY);
            this.rotation = rotation;
            this.size = new Vector2(width, height);
        }
        public OBB2D(float centerX, float centerY, float width, float height, double rotation = 0.0)
        {
            this.center = new Vector2(centerX, centerY);
            this.rotation = rotation;
            this.size = new Vector2(width, height);
        }

        /// <summary>
        /// Maybe not the fastest but the easiest to program
        /// </summary>
        public static bool Contains(OBB2D rec, Vector2 point)
        {
            Vector2 relPos = Calc.RotateAround(point, rec.center, -rec.rotationF) - rec.center;
            return (Calc.WithinBounds(-rec.Width / 2f, rec.Width / 2f, relPos.X)
                 && Calc.WithinBounds(-rec.Height / 2f, rec.Height / 2f, relPos.Y));
        }
        /// <summary>
        /// Maybe not the fastest but the easiest to program
        /// </summary>
        public bool Contains(Vector2 point)
        {
            Vector2 relPos = Calc.RotateAround(point, center, -rotationF) - center;
            return (Calc.WithinBounds(-Width / 2f, Width / 2f, relPos.X)
                 && Calc.WithinBounds(-Height / 2f, Height / 2f, relPos.Y));
        }

        public static bool OverlapsOneWay(OBB2D ths, OBB2D other)
        {
            //Loop through the axises (on this rectangle)
            for (int a = 0; a < 2; a++)
            {
                Vector2 axis = Vector2.Zero;
                if (a == 0)
                    axis = ths.AxisX;
                else
                    axis = ths.AxisY;
                float origin = Vector2.Dot(ths.TopLeft, axis);

                double t = Vector2.Dot(other.TopLeft, axis);
                double tMin = t;
                double tMax = t;
                //Loop through the corners
                for (int c = 0; c < 4; c++)
                {
                    Vector2 corner = other.GetCorner(c);

                    t = Vector2.Dot(corner, axis);

                    if (t < tMin)
                        tMin = t;
                    else if (t > tMax)
                        tMax = t;
                }

                if (tMin > 1 + origin || tMax < origin)
                {
                    //Found a dimension where they overlapped
                    return false;
                }
            }

            //There was no dimension where they didn't overlap
            return true;
        }
        public static bool Intersects(OBB2D rec1, OBB2D rec2)
        {
            return (OverlapsOneWay(rec1, rec2) && OverlapsOneWay(rec2, rec1));
        }
        public bool Intersects(OBB2D other)
        {
            return (OverlapsOneWay(this, other) && OverlapsOneWay(other, this));
        }

        public Vector2 GetCorner(int index)
        {
            switch (index)
            {
                case 0:
                    return TopLeft;
                case 1:
                    return TopRight;
                case 2:
                    return BottomLeft;
                default:
                    return BottomRight;
            };
        }
        public Range GetRangeOnAxis(Vector2 axis)
        {
            Range r = new Range(0, 0);
            r.min = float.MaxValue;
            r.max = float.MinValue;
            for (int i = 0; i < 4; i++)
            {
                float value = Vector2.Dot(GetCorner(i), axis);

                if (value < r.min)
                    r.min = value;
                if (value > r.max)
                    r.max = value;
            }

            return r;
        }
        public Vector2 FindMaxPointOnAxis(Vector2 axis)
        {
            float max = float.MinValue;
            int index = -1;
            for (int i = 0; i < 4; i++)
            {
                float dot = Vector2.Dot(GetCorner(i), axis);
                if (dot > max)
                {
                    max = dot;
                    index = i;
                }
            }

            if (index == -1)
            {
                Console.WriteLine("Something went wrong in FindMaxPointOnAxis. Did not find any points");
                return this.center;
            }

            return GetCorner(index);
        }
        public Vector2 FindMinPointOnAxis(Vector2 axis)
        {
            float max = float.MinValue;
            int index = -1;
            for (int i = 0; i < 4; i++)
            {
                float dot = Vector2.Dot(GetCorner(i), axis);
                if (dot > max)
                {
                    max = dot;
                    index = i;
                }
            }

            if (index == -1)
            {
                Console.WriteLine("Something went wrong in FindMaxPointOnAxis. Did not find any points");
                return this.center;
            }

            return GetCorner(index);
        }
        /// <summary>
        /// This checks for intersection between OBBs and finds the best normal to solve it
        /// as well as tells you the position where they meet after solving.
        /// </summary>
        /// <param name="amountToMove">Double's as a normal vector (if normalized)
        /// and the translation needed to resolve the collision
        /// This is a full translation and should be broken between the two objects
        /// depending on masses of each.</param>
        /// <param name="intersection">The point (in world coordinates) where the two 
        /// OBBs are touching</param>
        /// <returns>Whether they intersect</returns>
        public static bool IntersectionPoint(OBB2D rec1, OBB2D rec2, out Vector2 amountToMove, out Vector2 intersection)
        {
            Vector2[] axises = new Vector2[4]
            {
                Vector2.Normalize(rec1.AxisX), Vector2.Normalize(rec1.AxisY),
                Vector2.Normalize(rec2.AxisX), Vector2.Normalize(rec2.AxisY)
            };

            Range[] rec1Ranges = new Range[4];
            Range[] rec2Ranges = new Range[4];
            for (int i = 0; i < 4; i++)
            {
                rec1Ranges[i] = rec1.GetRangeOnAxis(axises[i]);
                rec2Ranges[i] = rec2.GetRangeOnAxis(axises[i]);
            }

            float minOverlap = float.MaxValue;
            int minOverlapAxis = -1;
            for (int i = 0; i < 4; i++)
            {
                float overlap = rec1Ranges[i].OverlapAmount(rec2Ranges[i]);
                if (Math.Abs(overlap) < Math.Abs(minOverlap))
                {
                    minOverlap = overlap;
                    minOverlapAxis = i;
                }
            }

            if (minOverlapAxis == -1)
            {
                Console.WriteLine("Something went wrong with finding the minimum intersect axis");
                amountToMove = Vector2.Zero;
                intersection = Vector2.Zero;
                return false;
            }

            Vector2 normal = axises[minOverlapAxis];

            Vector2 minOnAxisPoint = Vector2.Zero, maxOnAxisPoint = Vector2.Zero;
            {
                float minOnAxis = float.MaxValue, maxOnAxis = float.MinValue;
                int minOnAxisIndex = -1, maxOnAxisIndex = -1;
                #region Find Min and Max Points
                if (minOverlapAxis >= 2)
                {
                    //The axis came from rec1
                    for (int i = 0; i < 4; i++)
                    {
                        float dot = Vector2.Dot(rec1.GetCorner(i), normal);
                        if (dot < minOnAxis)
                        {
                            minOnAxis = dot;
                            minOnAxisPoint = rec1.GetCorner(i);
                            minOnAxisIndex = i;
                        }
                        if (dot > maxOnAxis)
                        {
                            maxOnAxis = dot;
                            maxOnAxisPoint = rec1.GetCorner(i);
                            maxOnAxisIndex = i;
                        }
                    }
                }
                else
                {
                    //The axis came from rec2
                    for (int i = 0; i < 4; i++)
                    {
                        float dot = Vector2.Dot(rec2.GetCorner(i), normal);
                        if (dot < minOnAxis)
                        {
                            minOnAxis = dot;
                            minOnAxisPoint = rec2.GetCorner(i);
                            minOnAxisIndex = i;
                        }
                        if (dot > maxOnAxis)
                        {
                            maxOnAxis = dot;
                            maxOnAxisPoint = rec2.GetCorner(i);
                            maxOnAxisIndex = i;
                        }
                    }
                }
                #endregion
            }

            if (minOverlap >= 0)
            {
                intersection = minOnAxisPoint;
                if (minOverlapAxis < 2)
                    intersection = maxOnAxisPoint;
            }
            else
            {
                intersection = maxOnAxisPoint;
                if (minOverlapAxis < 2)
                    intersection = minOnAxisPoint;
            }
            intersection += normal * minOverlap;

            amountToMove = normal * minOverlap;

            return true;
        }

        public static bool Intersects(OBB2D rec, Line2D line, out Vector2 intersection1, out Vector2? intersection2)
        {
            Line2D newLine = Line2D.RotateAround(line, rec.center, -rec.rotationF);
            newLine.p1 -= rec.center;
            newLine.p2 -= rec.center;
            Vector2 i1;
            Vector2? i2;
            bool intersects = Calc.LineVRectangle(new RectangleF(-rec.Width / 2f, -rec.Height / 2f, rec.Width, rec.Height), line, out i1, out i2);
            if (!intersects)
            {
                intersection1 = Vector2.Zero;
                intersection2 = null;
                return false;
            }
            intersection1 = Calc.RotateAround(i1, Vector2.Zero, rec.rotationF) + rec.center;
            if (i2.HasValue)
                intersection2 = Calc.RotateAround(i2.Value, Vector2.Zero, rec.rotationF) + rec.center;
            else
                intersection2 = null;
            return true;
        }
        public bool Intersects(Line2D line, out Vector2 intersection1, out Vector2? intersection2)
        {
            Line2D newLine = Line2D.RotateAround(line, center, -rotationF) - center;
            //newLine.p1 = Calc.RotateAround(line.p1, center, -rotationF) - center;
            //newLine.p2 = Calc.RotateAround(line.p2, center, -rotationF) - center;
            Vector2 i1;
            Vector2? i2;
            bool intersects = Calc.LineVRectangle(new RectangleF(-Width / 2f, -Height / 2f, Width, Height), newLine, out i1, out i2);
            if (!intersects)
            {
                intersection1 = Vector2.Zero;
                intersection2 = null;
                return false;
            }
            intersection1 = Calc.RotateAround(i1, Vector2.Zero, rotationF) + center;
            if (i2.HasValue)
                intersection2 = Calc.RotateAround(i2.Value, Vector2.Zero, rotationF) + center;
            else
                intersection2 = null;
            return true;
        }

        public static OBB2D Grow(OBB2D rec, float amount)
        {
            return new OBB2D(rec.center, rec.size + new Vector2(amount * 2, amount * 2), rec.rotation);
        }
        public void Grow(float amount)
        {
            size = new Vector2(size.X + amount * 2, size.Y + amount * 2);
        }
        public static OBB2D Translate(OBB2D rec, Vector2 amount)
        {
            return new OBB2D(rec.center + amount, rec.size, rec.rotation);
        }
        public void Translate(Vector2 amount)
        {
            center += amount;
        }

        public static explicit operator Polygon2D(OBB2D o)
        {
            Polygon2D poly = new Polygon2D(4);
            poly.verts[0] = o.TopLeft;
            poly.verts[1] = o.TopRight;
            poly.verts[2] = o.BottomRight;
            poly.verts[3] = o.BottomLeft;

            return poly;
        }
    }
}
