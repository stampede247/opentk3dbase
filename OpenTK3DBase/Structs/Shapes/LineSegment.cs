﻿using OpenTK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenTK3DBase
{
    /// <summary>
    /// Represents a 3D line segment starting at p1 and ending at p2
    /// </summary>
    public struct LineSegment
    {
        public Vector3 p1, p2;

        /// <summary>
        /// if p1.Y == p2.Y
        /// </summary>
        public bool IsHorizontal { get { return p1.Y == p2.Y; } }
        /// <summary>
        /// if p1 and p2 x and z values are the same
        /// </summary>
        public bool IsVertical { get { return (p1.X == p2.X && p1.Z == p2.Z); } }
        /// <summary>
        /// p2 - p1 normalized
        /// </summary>
        public Vector3 DirUnitVector { get { return Vector3.Normalize(p2 - p1); } }

        public LineSegment(Vector3 p1, Vector3 p2)
        {
            this.p1 = p1;
            this.p2 = p2;
        }

        /// <summary>
        /// Returns one of the 2 points.
        /// NOTE: 0 based index
        /// 
        /// Throws: ArgumentOutOfRangeException if < 0 or > 1
        /// </summary>
        /// <param name="index">0 or 2 (NOT 1 and 2!!)</param>
        public Vector3 GetPoint(int index)
        {
            switch (index)
            {
                case 0:
                    return p1;
                case 1:
                    return p2;

                default:
                    throw new ArgumentOutOfRangeException("index");
            };
        }
        /// <summary>
        /// Sets one of the 2 points.
        /// NOTE: 0 based index
        /// 
        /// Throws: ArgumentOutOfRangeException if < 0 or > 1
        /// </summary>
        /// <param name="index">0 or 2 (NOT 1 and 2!!)</param>
        /// <param name="value">The new value of the point</param>
        public void SetPoint(int index, Vector3 value)
        {
            switch (index)
            {
                case 0:
                    p1 = value;
                    break;
                case 1:
                    p2 = value;
                    break;

                default:
                    throw new ArgumentOutOfRangeException("index");
            };
        }
    }
}
