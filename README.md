# README #

Clone a local copy and then open with Visual Studios 2015 or later. You must have NuGet Package Manager working to restore OpenTK and other dependencies.

### What is this repository for? ###

* This is my basic starting template for all 3D projects in OpenTK. It's still a work in progress and there's no documentation besides the comments contained within.
* Version 1.0